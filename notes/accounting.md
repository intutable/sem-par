# Accounting

* [Commit statistics](https://gitlab.com/intutable/sem-par/-/graphs/develop)
  * Current state (2022-03-09) (see `git shortlog`)
    * 173 commits by Jakob Moser
    * 135 commits by Robin Mader
* [Closed issues](https://gitlab.com/intutable/sem-par/-/issues?sort=updated_desc&state=closed) have assignees to indicate who has done most of the work
  * Current state (2022-03-09)
    * 21 closed issues assigned to Robin Mader
    * 24 closed issues assigned to Jakob Moser

## By aspects

Most aspects had equal contributions of both team members (see e.g. the output of `git blame` given an arbitrary file, you will usually see both of our names), so it is hard to pin this down precisely. A few aspects had more contributions from one team member, which we tried to list here (however, most important parts, e.g. API design and implementation, were done in close agreement or even pair-programming).

* Original `semantic_pandas` Python code: Robin Mader
* Adaptation of original `semantic_pandas` Python code: Robin Mader
* Continuous Integration & Tooling: Robin Mader
* Documentation: Jakob Moser
* Wrap-up & Finalization: Jakob Moser
* Grammar & Evaluator extension: Jakob Moser
* API design: Both
* API implementation: Both
* Unit tests: Both
* Evaluation datasets: Both
* Bug fixing: Both

## Changes made to the original `semantic_pandas` code

* Remove unnecessary code (e.g. VSCode language server) from thesis
* Refactor to use Danfo instead of Pandas (affected sample queries, part of the grammar and the evaluator)
* Massive performance improvements
* Extension of grammar to handle more utterances

