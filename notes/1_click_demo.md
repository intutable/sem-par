# 1-Click Demo

To quickly verify that everything is working as it should, we recommend to use the Read-Evaluate-Print-Loop (REPL). 

The [contributing guide](../CONTRIBUTING.md) explains in detail how set up a development environment (including the REPL) on your machine, but if you encounter any problems during setup, you can also try to use our **Dockerized 1-Click Demo**.

This requires that you have a working Docker and a working Git installation. Execute the following commands to build the image and run a container:

```
git clone --recurse-submodules https://gitlab.com/intutable/sem-par
cd sem-par
sudo docker build -t sem-par .
sudo docker run -it sem-par
```

This will start the REPL.
