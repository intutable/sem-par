# Evaluation

For our project, we have decided to use a real-world evaluation measure, namely the utility it has to an end-user. Put differently: 

> Given a sensible scenario of what a user wants to do with their data, how much can they do using natural language (i.e. using only `sem-par`'s code suggestions)?

## Evaluation procedure

To best simulate such a "sensible scenario", we have decided to select a few hands-on data science tutorials that work on a concrete task using, preferably, Danfo.js, or, somewhat less preferably, Pandas.

Evaluation then works like this:

1. Check how many steps the tutorial has (i.e. how many lines of codes are executed)
2. Try to follow the tutorial only using our NL interface
3. How many of those steps can be done using NL?

The model's performance can then simply be defined as $\frac{\text{\#steps that can be done with NL}}{\text{\#steps in total}}$.

Currently, evaluation has to be performed by hand. We try to facilitate this by providing a Read-Evaluate-Print-Loop (REPL) so that a developer can check many utterances in a short time. Ideally, the REPL could save every entered utterance (maybe together with the expected code snippet), generating a small test dataset on the fly.

## Tutorials for evaluation

The following tutorials can be used for evaluation:

1. [Titanic Survival Prediction using Danfo.js and Tensorflow.js](https://danfo.jsdata.org/examples/titanic-survival-prediction-using-danfo.js-and-tensorflow.js), Danfo.js
2. [Getting Started](https://danfo.jsdata.org/getting-started), Danfo.js
3. [Danfo.js - Python Pandas like Library in JavaScript](https://www.youtube.com/watch?v=1YhGeiN2t04), Bhavesh Bhatt, YouTube
4. [Linear Regression on Boston Housing Dataset](https://towardsdatascience.com/linear-regression-on-boston-housing-dataset-f409b7e4a155?gi=f9a83b4d4102), Animesh Agarwal, Towards Data Science
