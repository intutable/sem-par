import * as fs from "fs"
import * as path from "path"
import { parse, Options } from "csv-parse"
import { Core } from "@intutable/core"
import { getSuggestions } from "../requests"
import { SuggestionsResponse } from "../types"

const DATASET_FOLDER: string = path.resolve(__dirname, "../../datasets")
const DATASET_FILE_ENDING = ".csv"
const DATASET_HEADERS: string[] = ["utterance", "snippet"]
const DATASET_DELIMITER = ","
const DATASET_ENCODING: BufferEncoding = "utf-8"
const SIMILARITY_THRESHOLD = 0.7

interface UtteranceSnippet {
    utterance: string
    snippet: string
}

interface Dataset {
    name: string
    data: UtteranceSnippet[]
    successfullyParsed: number
}

/**
 * Parse CSV file async and return typed results.
 * 
 * This is a thin, async wrapper for the `parse` function in `csv-parse`.
 * @param input Path to CSV file or file buffer
 * @param options Options for parsing
 * @template T The type of a single entry of the CSV file
 * @returns An array of all records in the CSV file
 */
async function parseCsvAsync<T>(
    input: string | Buffer,
    options?: Options | undefined
): Promise<T[]> {
    return await new Promise((resolve, reject) => {
        parse(input, options, (error, result: T[]) => {
            if (error) {
                reject(error)
            } else {
                resolve(result)
            }
        })
    })
}

async function loadDatasets(): Promise<Dataset[]> {
    // Get all file names with the corresponding ending from the dataset directory
    const datasetFileNames: string[] = fs.readdirSync(DATASET_FOLDER)
        .filter(file => file.endsWith(DATASET_FILE_ENDING))
    
    // Load and parse all CSV files into typed datasets
    const datasets: Dataset[] = []
    for (const datasetFileName of datasetFileNames) {
        const datasetFilePath = path.resolve(DATASET_FOLDER, datasetFileName)
        const datasetFileContent = fs.readFileSync(datasetFilePath, { encoding: DATASET_ENCODING })

        const datasetData: UtteranceSnippet[] = await parseCsvAsync<UtteranceSnippet>(
            datasetFileContent, {
                delimiter: DATASET_DELIMITER,
                columns: DATASET_HEADERS,
                fromLine: 2 // skip headers
            }
        )
        datasets.push({name: datasetFileName, data: datasetData, successfullyParsed: 0})
    }
    
    return datasets
}

async function main() {
    // Load intutable app core in order to access the
    // public API of the sem-par plugin
    const core = await Core.create(
        // Load the sem-par plugin
        [path.join(__dirname, "../../")]
    )

    const datasets: Dataset[] = await loadDatasets()

    // Process all loaded datasets
    for (const dataset of datasets) {
        for (const utteranceSnippet of dataset.data) {
            // Parse utterance
            const response = (await core.events.request(
                getSuggestions(utteranceSnippet.utterance, 10, SIMILARITY_THRESHOLD)
            )) as SuggestionsResponse
            const suggestions = response.suggestions
            const snippets = suggestions.map(s => s.snippet)
            const expectedSnippet = utteranceSnippet.snippet

            // Evaluate result
            if (snippets.includes(expectedSnippet)) {
                dataset.successfullyParsed++
            }
        }
        
        // Print results
        console.log(`${dataset.name}: ${dataset.successfullyParsed}/${dataset.data.length}`)
    }

    // Print total results
    const totalSuccessfullyParsed = datasets.map(d => d.successfullyParsed).reduce((a, b) => a + b)
    const totalDataLength = datasets.map(d => d.data.length).reduce((a, b) => a + b)
    console.log("-----------------------------")
    console.log(`TOTAL: ${totalSuccessfullyParsed}/${totalDataLength}`)

    // Clean up
    await core.plugins.closeAll()
}

void main()
