import * as inquirer from "inquirer"
import path from "path"
import * as fs from "fs"
import { Core } from "@intutable/core"
import { getSuggestions } from "../requests"
import { SuggestionsResponse } from "../types"
import commandLineArgs from "command-line-args"
import { OptionDefinition, CommandLineOptions } from "command-line-args"
import { createObjectCsvWriter } from "csv-writer"

/**
 * Command line arguments for the REPL. When not provided, a default value is assumed.
 */
const CLI_OPTIONS: OptionDefinition[] = [

    /**
     * The embedding similarity threshold over which two words are viewed as identical. 
     * When set to 1, only identical strings are treated as identical words.
     * When set to a value lower than 1, sem-par will try to understand unknown words by
     * finding similar words using word embeddings.
     */
    { name: "similarityThreshold", alias: "s", type: Number, defaultValue: 1 },
    
    /**
     * If the REPL should run in validate mode, i.e. ask the user for each utterance which
     * suggestion was correct (if any). Note this will automatically be assumed as true if
     * an output file is provided.
     */
    { name: "validate", type: Boolean },

    /**
     * To which output file the results of the validation (i.e. utterances and correct snippets) 
     * should be written. When none is provided, no output is written.
     */
    { name: "out", alias: "o", type: String },

    /**
     * The maximum number of suggestions the REPL should print out for each utterance.
     */
    { name: "numSuggestions", alias: "n", type: Number, defaultValue: 5 }
]

interface UserInput {
    input: string
}

async function main() {
    const args: CommandLineOptions = commandLineArgs(CLI_OPTIONS)

    const similarityThreshold = args["similarityThreshold"] as number
    const outFile = args["out"] as string
    const validate = outFile != undefined || args["validate"] as boolean
    const numSuggestions = args["numSuggestions"] as number

    const core = await Core.create(
        // Load the sem-par plugin
        [path.join(__dirname, "../../")]
    )

    let totalUtterances = 0
    let correctlyParsedUtterances = 0
    // NOTE: The REPL goes on indefinitely unless stopped, so we intentionally use while(true)
    // eslint-disable-next-line no-constant-condition
    while (true) {
        const { input: utterance } = (await inquirer.prompt([
            {
                name: "input",
                message: "What do you want to do?",
            },
        ])) as UserInput

        if (asksForExit(utterance)) {
            await core.plugins.closeAll()
            break
        }

        // Get suggestions for the utterance and print them
        const response = (await core.events.request(
            getSuggestions(utterance, numSuggestions, similarityThreshold)
        )) as SuggestionsResponse
        totalUtterances++

        for (let i = 0; i < response.suggestions.length; ++i) {
            const suggestion = response.suggestions[i]
            const printScore = suggestion.score.toFixed(2)
            console.log(`${i}: "${suggestion.snippet}" (score: ${printScore})`)
        }

        // Ask user if one of the suggestions is correct
        let expectedSnippet
        if (validate) {
            const { input: correctIndexString } = (await inquirer.prompt([
                {
                    name: "input",
                    message: "Which one of these suggestions is correct (leave empty if none are)?",
                },
            ])) as UserInput

            if (correctIndexString == "") {
                // Ask user what snippet they expected
                const { input: correctSnippet } = (await inquirer.prompt([
                    {
                        name: "input",
                        message: "What is the expected snippet?",
                    },
                ])) as UserInput

                expectedSnippet = correctSnippet
            }
            else {
                // Get correct snippet
                correctlyParsedUtterances++
                const correctIndex = parseInt(correctIndexString)
                expectedSnippet = response.suggestions[correctIndex].snippet
            }
        }

        // Write utterance and validated result to CSV file
        if (outFile != undefined && utterance != "") {
            // Append if file already exists, otherwise create new file
            const append = fs.existsSync(outFile)
            const csvWriter = createObjectCsvWriter({
                path: outFile,
                header: [
                    { id: "utterance", title: "utterance" },
                    { id: "snippet", title: "snippet" }
                ],
                append: append
            })
            await csvWriter.writeRecords([{
                "utterance": utterance,
                "snippet": expectedSnippet
            }])
        }
    }

    // Print final validation statistics
    if (validate) {
        console.log(
            `${correctlyParsedUtterances}/${totalUtterances} utterances parsed successfully.`
        )
    }
}

function asksForExit(utterance: string): boolean {
    const trimmed = utterance.toLowerCase().trim()
    return trimmed == "quit" || trimmed == "exit"
}

// main() is an async function, i.e. returns promise. However, we don't care about what
// it resolves to, so we ignore it using the `void` operator per ESlint's suggestions.
void main()
