/**
 * TypeScript bindings for our semantic parser written in Python.
 * 
 * This module is "one end of the bridge" connecting to the Python
 * module `semantic_pandas` that does the actual heavy lifting of
 * parsing the utterance, scoring the parse trees and generating
 * code snippets.
 * 
 * It uses the third-party `python-bridge` library to establish that
 * connection.
 * 
 * @module
 */
import { PythonBridge, pythonBridge } from "python-bridge"
import { Suggestion } from "./types"

export class SemanticParser {
    private python: PythonBridge
    private initialized: boolean = false

    public constructor(pythonExecutablePath: string = "python3") {
        this.python = pythonBridge({
            python: pythonExecutablePath
        })
    }

    /**
     * Ensure that the object has been initialized.
     * 
     * This function does not have to be called explicitly but
     * can be called to ensure that initialization is performed
     * at a convenient time. The object will initialize itself
     * otherwise.
     * If the object is already initialized this function
     * does nothing.
     */
    public async ensureInitializedAsync(): Promise<void> {
        if (!this.initialized)
            await this.initializeAsync()
    }

    /**
     * Initialize the object by preparing the Python environment.
     */
    private async initializeAsync(): Promise<void> {
        await this.python.ex`
            from semantic_pandas.intutable import IntutableDanfoParser
            parser = IntutableDanfoParser()
        `
        this.initialized = true
    }

    /**
     * Dispose all resources acquired by this object.
     * 
     * Namely this gracefully exits the internal Python process.
     * No further operations should be executed after this function
     * has been called.
     */
    public async disposeAsync(): Promise<void> {
        await this.python.end()
    }


    /**
     * Get suggestions which are similar to the utterance snippet pair.
     * @param utterance The natural language utterance corresponding to the snippet.
     * @param snippet The snippet to use for finding similar snippets.
     * @param similarityThreshold The threshold above which words are considered synonyms based on 
     *  their vector distance. Must be a value in [0, 1].
     * @returns A list of suggestions by their similarity to `snippet`.
     */
    public async getSimilarSuggestionsAsync(
        utterance: string, snippet: string, similarityThreshold: number = 1
    ): Promise<Suggestion[]> {
        await this.ensureInitializedAsync()

        const scoredSnippets = await this.python`parser.getSimilarSnippets(
                ${utterance}, ${snippet}, ${similarityThreshold}
            )` as [string, number][]
        
        return this.filterDuplicateSnippets(
            this.filterNullSnippets(this.mapToSuggestionsArray(scoredSnippets))
        )
    }

    /**
     * Parse a given utterance and return all possible parses.
     * @param utterance The natural language utterance to parse.
     * @param similarityThreshold The threshold above which words are considered synonyms based on 
     *  their vector distance. Must be a value in [0, 1].
     * @returns A list of suggestions ordered by their score.
     */
    public async getSuggestionsAsync(
        utterance: string, similarityThreshold: number = 1
    ): Promise<Suggestion[]> {
        await this.ensureInitializedAsync()

        const scoredSnippets = 
            await this.python`
                parser.parseAll(${utterance}, ${similarityThreshold})
            ` as [string, number][]

        return this.filterDuplicateSnippets(
            this.filterNullSnippets(this.mapToSuggestionsArray(scoredSnippets))
        )
    }

    /**
     * Maps a given array of [string, number] pairs to an array of
     * Suggestion objects by interpreting the string as the code snippet
     * and the number as its probability score
     * @param rawData An array of arrays containing a string and a number
     */
    private mapToSuggestionsArray(rawData: [string, number][]): Suggestion[] {
        return rawData.map((s: [string, number]) => { return {
            snippet: s[0],
            score: s[1]
        }})
    }

    /**
     * Given a list of suggestions, returns a list that contains only suggestions
     * where all snippets are mutually different. If the original list contained
     * the same snippet more than once, the maximum score is chosen as score for
     * this snippet in the new list.
     * @param suggestions An array of suggestions, where the same snippet might occur more than once
     */
    private filterDuplicateSnippets(suggestions: Suggestion[]): Suggestion[] {
        const filteredSuggestions: Record<string, Suggestion> = {}

        for(const s of suggestions) {
            if (
                filteredSuggestions[s.snippet] === undefined ||
                filteredSuggestions[s.snippet].score < s.score
            ) {
                filteredSuggestions[s.snippet] = s
            }
        }

        return Object.values(filteredSuggestions)
    }

    /**
     * Given a list of suggestions, returns a list of suggestions that contains
     * only suggestions that are not null.
     * @param suggestions An array of suggestions
     * @returns An array of suggestions where no snippet is null
     */
    private filterNullSnippets(suggestions: Suggestion[]): Suggestion[] {
        return suggestions.filter(s => s.snippet !== null)
    }
}

/**
 * Create a SemanticParser.
 * 
 * For this, a lazy-initialized Python bridge will be created. You can
 * specify the Python executable to use in the environment variable
 * `PYTHON`. If none is specified, `python3` (from the OS'es `PATH`)
 * is used.
 */
export function getParser(): SemanticParser {
    const pythonExecutablePath = process.env.PYTHON || "python3"
    return new SemanticParser(pythonExecutablePath)
}
