/**
 * This module provides **request builder functions** for our API endpoints.
 * 
 * All functions in this file take as parameters whatever parameters the API endpoint
 * requires and return a request object containing those parameters and some
 * metadata. The request object can then be passed to `core` to receive a response
 * object, which contains whichever response the API sent.
 * 
 * To keep it simple, we talk about our functions like they would immediately return the
 * results contained in the response (e.g. "Returns 5 suggestions for the given query"),
 * although they actually only return a request object. To be fully technically correct,
 * all function documentation would have to be of the form "Returns a request object to
 * request a response which contains 5 suggestions for the given query".
 * 
 * @module
 */
import { SimilarSuggestionsRequest, SuggestionsRequest } from "./types"

const CHANNEL = "sem-par"

/**
 * Returns at maximum `maxSuggestions` suggestions for the given `utterance`. 
 * A suggestion consists of a code snippet and a probability score.
 * 
 * @param utterance A natural language string like "sort students by name"
 * @param maxSuggestions the maximum number of suggestions you want to receive (≥ 0).
 * @param similarityThreshold The threshold (in [0, 1]) to determine synonyms based on the
 *  embedding distance of given words
 * @returns a request that can be passed to core to receive a {@link SuggestionsResponse}.
 */
export function getSuggestions(
    utterance: string,
    maxSuggestions: number,
    similarityThreshold: number = 1
): SuggestionsRequest {
    return {
        channel: CHANNEL,
        method: "getSuggestions",
        utterance,
        maxSuggestions,
        similarityThreshold
    }
}

/**
 * Returns at maximum `maxSuggestions` suggestions for the given `utterance` 
 * that are similar to the given `snippet`. A suggestion consists of a code
 * snippet and a probability score.
 * 
 * @param utterance a natural language string like "sort students by name".
 * @param snippet a snippet the returned snippet should be
 *  similar to, e.g. "students.sort({ by: 'name' })".
 * @param maxSuggestions the maximum number of suggestions you want to receive (≥ 0).
 * @param similarityThreshold The threshold (in [0, 1]) to determine synonyms based on the
 *  embedding distance of given words
 * @returns a request that can be passed to core to receive a {@link SuggestionsResponse}.
 */
export function getSimilarSuggestions(
    utterance: string,
    snippet: string,
    maxSuggestions: number,
    similarityThreshold: number = 1
): SimilarSuggestionsRequest {
    return {
        channel: CHANNEL,
        method: "getSimilarSuggestions",
        utterance,
        snippet,
        maxSuggestions,
        similarityThreshold
    }
}
