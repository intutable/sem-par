/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
    preset: "ts-jest",
    testEnvironment: "node",
    collectCoverageFrom: [
        "src/**/*.ts"
    ],
    coverageDirectory: "coverage",
    coverageReporters: ["html", "cobertura", "text"],
    coveragePathIgnorePatterns: ["/node_modules/", "/dist/"],
    reporters: [
        "default",
        [
            "jest-junit", {
                outputDirectory: "coverage",
                outputName: "jest-junit.xml"
            }
        ]
    ]
}
