# Contributing Guide

You want to contribute to sem-par? Great! This guide includes all relevant information to get you started.

[[_TOC_]]

## 📔 How To Read This Manual

While reading this guide, please note that it was written for a Linux environment. On Windows and macOS, some of the setup steps may differ – we tried to document the differences to the extent of our knowledge.

Throughout the guide, we use the following icons to mark sections of special importance:

* 🔄: **Things you have to repeatedly do** in the development process (e.g. every time after making a change or before starting the application). If something suddenly and inexplicably doesn't work some day, ensure that you haven't forgot to do one of those repeated tasks.
* ⚠️: **Things to watch out for**. Do this to avoid getting errors and making mistakes during setup or development. We mark everything known to cause problems with this icon.
  * 🪟: Subcategory for things you (only) have to watch out for when you are on Windows.
  * 🍎: Subcategory for things you (only) have to watch out for when your are on macOS.
* 🪲: **Errors and how to fix them**. If, despite watching out, you get an error, have a look at sections marked with this icon. Common errors are described here (usually with an error message) and how we managed to fix them.

Additional information is sometimes hidden behind a dropdown like this:

<details>
<summary>Even more details about this manual</summary>

Have a look at this [webcomic](https://xkcd.com/1343/).
</details>

Click the small triangle to the left or the text right next to the triangle to expand and collapse such a section.

## 🔧 Prerequisites and Setup

You will need the following tools to set up a development environment:

* [Git][git-scm]
* [Node.js][node]
* [Python][python]

<details>
<summary>🪟 Pay attention to the PATH variable</summary>

Especially on Windows, the PATH environment variable is often not set correctly. Furthermore, in the newer Windows versions there are multiple ways to install programs (e.g. Python) that behave differently. If something does not work correctly, you might need to adjust the PATH environment variable (e.g. to point to the executable if a command doesn't work out-of-the-box; or to point to the _correct_ executable if there are multiple versions installed at the same time).

We sadly cannot provide a comprehensive guide here, but keep this in mind should you encounter any problems.
</details>

After installing the tools, clone the repository to create a local working copy of the code:

```bash
git clone --recurse-submodules https://gitlab.com/intutable/sem-par.git
```

<details>
<summary>🪟 Different command required on Windows</summary>

On Windows, you need to instead execute the following command **as an administrator**, otherwise symlinks won't work.

```bash
git clone --recurse-submodules -c core.symlinks=true https://gitlab.com/intutable/sem-par.git
```
</details>

Change into the cloned directory:

```
cd sem-par
```

You then need to install the node.js and Python code. We recommend creating a virtual environment for the Python code (however, this is not required, see the variant at the end of this section). This can all be done with the following lines:

```bash
npm install
```

```bash
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip setuptools wheel
pip install -e ./python/.[dev]
```

🔄 You need to execute `source venv/bin/activate` **every time before running `sem-par` code** (or, more precisely, once per shell session).

<details>
<summary>🪟🍎 Different commands required on Windows and macOS</summary>

Instead of the `source venv/bin/activate`, you will need a different command to activate the virtual environment. The [documentation of venv][venv-docs] has a table listing the right one for your system and shell.

</details>

<details>
<summary>🪲 Could not install packages due to an OSError.</summary>

If during the execution of the `pip install` commands you get the following error, try again as administrator.

```
ERROR: Could not install packages due to an OSError: [WinError 5] Permission denied [...]
Check the permissions.
```
</details>

<details>
<summary>⚠️ Ensure that the correct Python executable is used.</summary>

Apparently sometimes, despite activating the venv, the wrong Python executable is used. We especially encountered this on Windows systems.

To ensure that that the correct Python executable is used, set the `PYTHON` environment variable to the path of the Python executable.

In Bash, you can do this by executing: `PYTHON=venv/bin/python`. In PowerShell on Windows, `$env:PYTHON=".\venv\Scripts\python.exe"` seems to work.
</details>

<details>
<summary>Variant: Not using a virtual environment.</summary>

While using a venv is recommended, it is not required. If you run into any problems during setup that cannot easily be fixed, you can also try to set everything up without a venv.

In this case, perform setup as described above, but omit the lines:

```
python3 -m venv venv
source venv/bin/activate
```

You then also need not execute `source venv/bin/activate` before executing the application.

</details>

## 🏗️ Building the Application

To build the application, execute:

```
npm run build
```

If everything works, only the following lines will be shown before the shell prompt appears again (the version number might differ):

```
> @intutable/sem-par@1.1.0 build
> tsc
```

Even though there is no explicit success message: If you see this, don't worry, everything is fine.

🔄 You need to execute `npm run build` **every time you change the TypeScript code**, otherwise the changes won't appear when running the program. Note that changing the Python code under the hood does not require a rebuild.

<details>
<summary>🪲 The command outputs the tsc help.</summary>

If, on the other hand, you get a lengthy help output by "tsc: The TypeScript Compiler" explaining the compiler options, you have probably not cloned the Git submodules when cloning the main repository. The compiler configuration is stored in a submodule, so in this case, it cannot actually compile anything and will only show the help prompt.

To fix this, execute the following commands and then try to build again:

```
git submodule init
git submodule update
```

</details>

## 🏃 Run

In production, you will probably only interact with `sem-par` through an xTable GUI (which is not covered in this guide). However, for development we provide a Read-Evaluate-Print-Loop (REPL) and an evaluation script to quickly run and test the application.

### REPL

The Read-Evaluate-Print-Loop is an easy way to test the entire application. You will probably use it a lot during development. Start it using the following command:

```bash
npm run repl
```

You will then be prompted to input a natural language utterance. Enter one and press <kbd>Enter</kbd>. The application will then show some code suggestions and ask you for a new utterance (and so on).

If you've entered enough utterances, type `exit` to exit the repl.

Hint: You can provide a few command line arguments to the REPL (see the `CLI_OPTIONS` documentation in the respective TypeScript file for details). At least on Linux, when executing the REPL via npm, you need to add `--` after `npm run repl` to ensure that npm passes the arguments on to the REPL script and doesn't try to parse them itself. To provide e.g. the `-s 0.7` parameter, you'd have to execute:

```bash
npm run repl -- -s 0.7
```

<details>
<summary>🪲 Application crashes after entering utterance.</summary>

If you haven't run `npm run build` at least once, the following message will occur:

```
[UnhandledPromiseRejection: This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). The promise rejected with the reason "#<Object>".] {
  code: 'ERR_UNHANDLED_REJECTION'
}
```

To fix it, simply run `npm run build` and then try again.

If the program crashes with an error message containing the phrase `No module named 'semantic_pandas'`, the venv is probably not active. This can have at least two reasons:

1. You simply forgot to activate it.
2. The system is not using the correct Python executable despite having enabled the venv. See "Prerequisites and Setup" for a fix.

If the program crashes with an error message containing the phrase `DLL load failed` (on Windows), you might need to install a Visual C++ redistributable, see [this Stack Overflow question](https://stackoverflow.com/questions/68261191/importerror-dll-load-failed-while-importing-strings-the-specified-module-could).
</details>

### Evaluator

The evaluator will evaluate the performance of `sem-par` against a few hand-crafted datasets with real-life-like utterances and output a score (how many utterances resulted in a useful suggestion?). Execute it using:

```bash
npm run evaluate
```

## 🏛️ Repository Structure

* `src/` contains the TypeScript source code for the application.
  * `index.ts` contains the main initialization function that makes `sem-par` listen to requests received via core and handler functions for those requests.
  * `requests.ts` defines the request builder functions that form the public API of the plugin.
  * `types.ts` defines the types of responses the application can send (which are also part of the public API of the plugin).
  * `semanticParser.ts` connects to the Python code that does the actual heavy lifting. It is "one end of the bridge" connecting TypeScript and Python code.
  * `util/` contains the code for REPL and Evaluator.
* `python/semantic_pandas/` contains the Python source code for the application. This code is responsible for parsing utterances and generating code suggestions.
  * `intutable/intutable_danfo_parser.py` is "the other end of the bridge" connecting TypeScript and Python. Its methods are called by `semanticParser.ts`.
  * `grammars/danfo_grammar.py` contains the grammar that can parse NL utterances, more on that later.
  * `evaluators/danfo_semantic_evaluator.py` contains the evaluator that can convert parses into Danfo.js code snippets.
  * `datasets/` contains hand-crafted datasets to train the parse scorer machine learning models.
  * `scripts/bake_data.py` trains the parse scorer machine learning models.
* `datasets/` contains datasets to evaluate the performance of `sem-par` on.

This list should give you a rough overview over the entire project. 

For more details, please see the documentation comments in the respective files. If you want to know more about our API, you can also check out the API documentation at [intutable.gitlab.io/sem-par][typedoc], which is automatically generated from the documentation comments, but nicer to look at.

## 🧩 Extending the Grammar and Evaluator

If you want `sem-par` to understand more utterances, you have to make the following changes:

### Extend `python/grammars/danfo_grammar.py`

The grammar converts the NL utterance to an abstract semantic representation. The abstract semantic representation is developed by us to represent meaning in a machine-readable way.

Although one could argue that danfo.js code is also a machine-readable abstract semantic representation, this grammar does not know what danfo.js code looks like (creating code snippets is the evaluator's task). This makes it easier to adapt the system for other code languages (because you shouldn't have to change much about the grammar).

The grammar consists of non-terminal symbols like `$Operation`, `$SelectOperation`, `$SelectTypeArgument` or `$Append` and terminal symbols like `read`, `select`, `append`, `to` (those are the actual NL words you type in) which are combined to rules using the `makeRule` function. Rules can have `Semantic` objects assigned (e.g. `SimpleSemantic({"operation": "append"})`), which are used to build the semantic representation.

### Extend `python/grammars/danfo_semantic_evaluator.py`

The evaluator converts the abstract semantic representation to real danfo.js. It receives a `Parse` as input and can access its semantic, which might look like that:

```python
{'domain': 'danfo', 'operation': 'sort', 'typeVariable': {'variable': 'dataframe'}, 'by': {'variable': '"name"'}}
```

It then returns a string of danfo.js code, like:

```ts
dataframe.sortValues("name", { inplace: true })
```

### Train new scorer models

If you change the rules of the grammar, the parse scoring machine learning model needs to be retrained (because its features are generated from the grammars' rules).

You therefore need to delete the old models (stored in `python/semantic_pandas/resources`) and "bake" new models, using the `bake_data` utility script:

```
python3 -m semantic_pandas.scripts.bake_data md 30
```

The parameter `30` is the number of features the resulting vectors after reducing should have.

### Test it

After doing all this, you should be ready to go! Note you do not need to rebuild (because you only changed python code), so simply run the REPL and see if the extension worked.

## 💻 Contribute Code

This project adheres to the [xTable development guidelines][development-guidelines]. If you want to add or change code, the basic procedure is this:

1. Open an issue describing what you want to do (e.g. `fix: don't crash on negative integer input`).
2. Create a feature branch (e.g. `fix/dont-crash-on-negative-integer-input`) for the issue.
3. Open a merge request for the feature branch and mark it as draft.
4. As soon as you're done, mark the merge request as ready.
5. A reviewer will approve the merge request (or ask for changes).
6. Finally, the merge request is merged into `develop`.

[git-scm]: https://git-scm.com/
[node]: https://nodejs.org/
[python]: https://www.python.org/downloads/
[venv-docs]: https://docs.python.org/3/library/venv.html
[development-guidelines]: https://intutable.gitlab.io/intutable/contributing/
[typedoc]: https://intutable.gitlab.io/sem-par/
