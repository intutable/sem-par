# This Dockerfile is only relevant for our 1-click demo, see notes/1_click_demo.md
# If you want to use the plugin or contribute to its development, you will likely
# never have to use this.
FROM node:latest

WORKDIR /sem-par

RUN apt-get update && apt-get install -y \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*

COPY ./shared-config/.npmrc ./.npmrc
COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json
COPY ./shared-config/tsconfig.json ./tsconfig.json

COPY ./datasets ./datasets
COPY ./python/setup.py ./python/setup.py
COPY ./python/semantic_pandas ./python/semantic_pandas
COPY ./src ./src

RUN pip install -e /sem-par/python

RUN npm install
RUN npm run build

# Directly launch the REPL when starting a container
CMD ["npm", "run", "repl"]
