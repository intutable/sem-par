import process from "process"
import { PythonBridge, pythonBridge } from "python-bridge"

let python: PythonBridge
    
// Increase timeout since python processes
// requires some extra time to import/dipose packages
jest.setTimeout(50000)

beforeEach(() => {
    python = pythonBridge({
        python: process.env.PYTHON || "python3"
    })
    
})

afterEach(async () => {
    await python.end()
})

describe("can use semantic_pandas Python package", () => {
    test("can import semantic_pandas", async () => {
        await python.ex`import semantic_pandas`
    })

    test("can get suggestions", async () => {
        const utterance = "new dataframe"
        const expected_snippet = "new danfo.DataFrame()"

        // load and set up all required objects
        await python.ex`
            from semantic_pandas.intutable import IntutableDanfoParser

            parser = IntutableDanfoParser()
        `
        const snippet = await python`parser.parse(${utterance})[0]` as string

        expect(snippet).toBe(expected_snippet)
    })
})
