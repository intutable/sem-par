import path from "path"
import { Core } from "@intutable/core"
import { getSimilarSuggestions, getSuggestions } from "../src/requests"
import { SuggestionsResponse } from "../src/types"
import { setTimeout } from "timers/promises"

// Increase timeout since python processes
// require some extra time to import/dipose packages
jest.setTimeout(50000)

let core: Core

beforeAll(async () => {
    core = await Core.create([path.join(__dirname, "../")])
})

afterAll(async () => {
    await core.plugins.closeAll()
    
    // HERE BE DRAGONS:
    // Due to a bug in jest or the python-bridge module
    // we need to wait a little bit because otherwise
    // jest will detect a phantom open handle to the 
    // python process
    await setTimeout(1000, null)
})

describe("can suggest code snippets", () => {
    test("returns suggestions", async () => {
        const { suggestions } =
            await core.events.request(getSuggestions("create dataframe", 5)) as SuggestionsResponse

        expect(suggestions.length).toBeGreaterThan(0)
    })

    test("orders suggestions by score", async () => {
        const { suggestions } =
            await core.events.request(getSuggestions("create dataframe", 5)) as SuggestionsResponse

        const scores = suggestions.map(it=>it.score)

        // .sort() sorts the array in place and returns a reference to it,
        // so we need to copy the array first.
        // Because .sort() sorts by ascending character order, we need a
        // custom comparator function
        const expectedScores = [...scores].sort((a, b) => b - a)
        expect(scores).toEqual(expectedScores)
    })

    test.each([0, 1, 2, 3, 4, 5, 6])(
        "respects maxSuggestions=%d parameter", async (maxSuggestions) => {
            const { suggestions } = await core.events.request(
                getSuggestions("create dataframe", maxSuggestions)
            ) as SuggestionsResponse

            expect(suggestions.length).toBeLessThanOrEqual(maxSuggestions)
        }
    )

    test("fails on maxSuggestions negative", async () => {
        await expect(
            core.events.request(getSuggestions("create dataframe", -1))
        ).rejects.toThrow(RangeError)
    })
})

describe("can suggest similar code snippets", () => {
    test("returns suggestions", async () => {
        const { suggestions } = (await core.events.request(
            getSimilarSuggestions("get minimum from col in df as min", "df[col].min()", 5)
        )) as SuggestionsResponse

        expect(suggestions.length).toBeGreaterThan(0)
    })

    test("orders suggestions by score", async () => {
        const { suggestions } = (await core.events.request(
            getSimilarSuggestions("get minimum from col in df as min", "df[col].min()", 5)
        )) as SuggestionsResponse

        const scores = suggestions.map(it => it.score)

        // .sort() sorts the array in place and returns a reference to it,
        // so we need to copy the array first.
        // Because .sort() sorts by ascending character order, we need a
        // custom comparator function
        const expectedScores = [...scores].sort((a, b) => b - a)
        expect(scores).toEqual(expectedScores)
    })

    test.each([0, 1, 2, 3, 4, 5, 6])(
        "respects maxSuggestions=%d parameter",
        async maxSuggestions => {
            const { suggestions } = (await core.events.request(
                getSimilarSuggestions(
                    "get minimum from col in df as min",
                    "df[col].min()",
                    maxSuggestions
                )
            )) as SuggestionsResponse

            expect(suggestions.length).toBeLessThanOrEqual(maxSuggestions)
        }
    )

    test("fails on maxSuggestions negative", async () => {
        await expect(
            core.events.request(
                getSimilarSuggestions("get minimum from col in df as min", "df[col].min()", -1)
            )
        ).rejects.toThrow(RangeError)
    })
})
