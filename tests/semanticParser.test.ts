import { getParser, SemanticParser } from "../src/semanticParser"
import { Suggestion } from "../src/types"
import { setTimeout } from "timers/promises"

let parser: SemanticParser

// Increase timeout since python processes
// require some extra time to import/dipose packages
jest.setTimeout(50000)

beforeEach(() => {
    parser = getParser()
})

afterEach(async () => {
    await parser.disposeAsync()

    // HERE BE DRAGONS:
    // Due to a bug in jest or the python-bridge module
    // we need to wait a little bit because otherwise
    // jest will detect a phantom open handle to the 
    // python process
    await setTimeout(1000, null)
})

describe("can use SemanticParser", () => {
    test("can get suggestions", async() => {
        const utterance = "new dataframe"
        const expected_snippet = "new danfo.DataFrame()"

        const suggestions: Suggestion[] = await parser.getSuggestionsAsync(utterance)

        expect(suggestions.length).toBeGreaterThan(0)
        expect(suggestions.map(s => s.snippet)).toContain(expected_snippet)

        for (const s of suggestions) {
            expect(s.score).toBeGreaterThanOrEqual(0)
            expect(s.score).toBeLessThanOrEqual(1)
        }
    })

    test("can parse with similarity", async () => {
        const utterance = "generate dataframe"
        const expected_snippet = "new danfo.DataFrame()"

        const suggestions: Suggestion[] = await parser.getSuggestionsAsync(utterance, 0.5)

        expect(suggestions.length).toBeGreaterThan(0)
        expect(suggestions.map(s => s.snippet)).toContain(expected_snippet)

        for (const s of suggestions) {
            expect(s.score).toBeGreaterThanOrEqual(0)
            expect(s.score).toBeLessThanOrEqual(1)
        }
    })

    test("can not parse without similarity", async () => {
        const utterance = "generate dataframe"
        const not_expected_snippet = "new danfo.DataFrame()"

        const suggestions: Suggestion[] = await parser.getSuggestionsAsync(utterance, 1)

        expect(suggestions.length).toBe(0)
        expect(suggestions.map(s => s.snippet)).not.toContain(not_expected_snippet)
    })

    test("can get similar parses", async () => {
        const utterance = "get minimum from col in df as min"
        const snippet = "df[col].min()"
        const expectedSimilarSnippet = "let min = %dataframe%.min()"
        
        const similarSuggestions: Suggestion[] =
                await parser.getSimilarSuggestionsAsync(utterance, snippet)
        const similarSnippets = similarSuggestions.map(s => s.snippet)

        expect(similarSnippets).toContain(expectedSimilarSnippet)
        expect(similarSnippets).not.toContain(snippet)
    })

    test("returns no duplicate snippets", async () => {
        const utterance = "create dataframe"
        const suggestions: Suggestion[] = await parser.getSuggestionsAsync(utterance, 10)

        const snippets = suggestions.map(s => s.snippet)
        const snippetsNoDuplicates = new Set(snippets)

        expect(snippets.length).toEqual(snippetsNoDuplicates.size)
    })

    test("returns no null suggestions", async () => {
        const utterance = "create dataframe"
        const suggestions: Suggestion[] = await parser.getSuggestionsAsync(utterance, 10)

        const snippets = suggestions.map(s => s.snippet)
        
        expect(snippets).not.toContain(null)
    })
})
