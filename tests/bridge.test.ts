import process from "process"
import { PythonBridge, pythonBridge } from "python-bridge"

let python: PythonBridge

beforeEach(() => {
    python = pythonBridge({
        python: process.env.PYTHON || "python3"
    })
})

afterEach(async () => {
    await python.end()
})

describe("can execute Python code", () => {
    test("can get result of calculation", async () => {
        const result = await python`40 + 2` as number
        expect(result).toBe(42)
    })

    test("uses Python 3", async () => {
        await python.ex`import sys`
        const major = await python`sys.version_info.major` as number
        expect(major).toBe(3)
    })
})
