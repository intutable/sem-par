"""
Class module for the `Rule` class.
Please see `help(Rule)` for more information.
"""

from abc import ABC
from typing import Iterable, Tuple

from ..semantics.semantic import Semantic
from ..semantics.empty_semantic import EmptySemantic
from ..utility import isOptional


class Rule(ABC):
    """Abstract base class for rule data classes."""
    def __init__(self, lhs: str, rhs: Iterable[str], semantic: Semantic = None):
        """Please see `help(Rule)` for more information."""

        if semantic is None:
            semantic = EmptySemantic()

        if not isinstance(semantic, Semantic):
            raise TypeError(
                f"semantic must be of type 'Semantic'. "
                f"Received {type(semantic).__name__}"
            )

        self.lhs: str = lhs
        self.rhs: Tuple[str] = tuple(rhs)
        self.semantic: Semantic = semantic

    def __str__(self) -> str:
        return 'Rule' + str((self.lhs, ' '.join(self.rhs), self.semantic))

    def containsOptional(self) -> bool:
        """Determine wether this rule contains a token/category marked as optional on its rhs."""
        return any(isOptional(rhs) for rhs in self.rhs)
