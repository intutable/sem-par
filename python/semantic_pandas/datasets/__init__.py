"""Module containing datasets for training/testing semantic parsers."""

from .other_queries import (
    otherQueries,
    trainingOtherQueries,
    testingOtherQueries
)

from .manual_danfo_queries import (
    manualDanfoQueries,
    trainingManualDanfoQueries,
    testingManualDanfoQueries
)

__all__ = [
    "otherQueries",
    "trainingOtherQueries",
    "testingOtherQueries",
    "manualDanfoQueries",
    "trainingManualDanfoQueries",
    "testingManualDanfoQueries"
]
