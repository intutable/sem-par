"""Module containing handcrafted danfo queries dataset for parser testing/training."""

import random
from collections import namedtuple


DanfoQuery = namedtuple("DanfoQuery", ["intent", "snippet"])


random.seed(3042330287)  # use seed for reproducable results


manualDanfoQueries = [
    # create dataframe
    DanfoQuery("new dataframe", "new danfo.DataFrame()"),
    DanfoQuery("new dataframe and assign to df", "let df = new danfo.DataFrame()"),
    DanfoQuery("create new dataframe", "new danfo.DataFrame()"),
    DanfoQuery("create dataframe from dict", "new danfo.DataFrame(dict)"),
    DanfoQuery("create dataframe df from dict", "let df = new danfo.DataFrame(dict)"),
    DanfoQuery(
        "create dataframe df from dict with index ind",
        "let df = new danfo.DataFrame(dict, { index: ind })"
    ),
    DanfoQuery(
        "create dataframe df from dict with index",
        "let df = new danfo.DataFrame(dict, { index: %index% })"
    ),

    # select column(s)
    #   single column
    DanfoQuery("select column from dataframe", "%dataframe%[%column%]"),
    DanfoQuery("select column from dataframe as result", "let result = %dataframe%[%column%]"),
    DanfoQuery("retrieve column from dataframe", "%dataframe%[%column%]"),
    DanfoQuery("get column", "%dataframe%[%column%]"),
    DanfoQuery("select column col from dataframe", "%dataframe%[col]"),
    DanfoQuery("select column 'colName' from dataframe", "%dataframe%['colName']"),
    DanfoQuery(
        "select column 'colName' from dataframe and assign to result",
        "let result = %dataframe%['colName']"
    ),
    DanfoQuery("get column 'colName' from dataframe", "%dataframe%['colName']"),
    DanfoQuery("select column from dataframe df", "df[%column%]"),
    DanfoQuery("select column from df", "df[%column%]"),
    DanfoQuery("get column 'id' from dataframe df", "df['id']"),
    DanfoQuery("get column 'id' from dataframe df as result", "let result = df['id']"),
    DanfoQuery("get column col from dataframe df", "df[col]"),
    DanfoQuery("select columns col from dataframe df", "df[col]"),
    DanfoQuery("retrieve columns 'colName' from dataframe df", "df['colName']"),

    #   multiple columns
    DanfoQuery(
        "select columns 'col1', 'col2' from dataframe df",
        "df.loc({ columns: ['col1', 'col2'] })"
    ),
    DanfoQuery(
        "select columns 'col1', 'col2' from dataframe df as result",
        "let result = df.loc({ columns: ['col1', 'col2'] })"
    ),
    DanfoQuery(
        "get columns 'col1', 'col2' from dataframe df",
        "df.loc({ columns: ['col1', 'col2'] })"
    ),
    DanfoQuery(
        "get columns 'col1', 'col2' from df",
        "df.loc({ columns: ['col1', 'col2'] })"
    ),
    DanfoQuery(
        "retrieve columns 'col1', 'col2' from dataframe df",
        "df.loc({ columns: ['col1', 'col2'] })"
    ),
    DanfoQuery(
        "retrieve columns 'col1', 'col2' from dataframe df as result",
        "let result = df.loc({ columns: ['col1', 'col2'] })"
    ),
    DanfoQuery(
        "retrieve columns 'col1', 'col2' from dataframe df and assign to result",
        "let result = df.loc({ columns: ['col1', 'col2'] })"
    ),

    # select rows
    #   by logical condition
    DanfoQuery("select rows", "%dataframe%.iloc({ rows: %condition% })"),
    DanfoQuery("select rows from dataframe", "%dataframe%.iloc({ rows: %condition% })"),
    DanfoQuery(
        "retrieve rows from dataframe as result",
        "let result = %dataframe%.iloc({ rows: %condition% })"
    ),
    DanfoQuery("select rows from dataframe df", "df.iloc({ rows: %condition% })"),
    DanfoQuery("get rows from dataframe df", "df.iloc({ rows: %condition% })"),
    DanfoQuery(
        "get rows from dataframe df as result",
        "let result = %dataframe%.iloc({ rows: %condition% })"
    ),
    DanfoQuery(
        "select rows where var1 greater than var2",
        "%dataframe%.iloc({ rows: %dataframe%[var1].gt(%dataframe%[var2]) })"
    ),
    DanfoQuery(
        "select rows where var1 greater than var2 and assign to result",
        "let result = %dataframe%.iloc({ rows: %dataframe%[var1].gt(%dataframe%[var2]) })"
    ),
    DanfoQuery(
        "select rows where var1 is greater than var2",
        "%dataframe%.iloc({ rows: %dataframe%[var1].gt(%dataframe%[var2]) })"
    ),
    DanfoQuery(
        "retrieve rows where var1 is greater than var2",
        "%dataframe%.iloc({ rows: %dataframe%[var1].gt(%dataframe%[var2]) })"
    ),
    DanfoQuery(
        "select rows from df where var1 greater than var2",
        "%dataframe%.iloc({ rows: %dataframe%[var1].gt(%dataframe%[var2]) })"
    ),
    DanfoQuery(
        "select rows from df where var1 greater than var2 and assign to result",
        "let result = %dataframe%.iloc({ rows: %dataframe%[var1].gt(%dataframe%[var2]) })"
    ),
    DanfoQuery(
        "get rows from df where var1 greater than var2",
        "%dataframe%.iloc({ rows: %dataframe%[var1].gt(%dataframe%[var2]) })"
    ),

    #   by position
    DanfoQuery("select rows 10 to 20", "%dataframe%.iloc({ rows: ['10:20'] })"),
    DanfoQuery("select rows from dataframe df 5 to 15", "%dataframe%.iloc({ rows: ['5:15'] })"),
    DanfoQuery(
        "select rows from dataframe df 5 to 15 and assign to range",
        "let range = %dataframe%.iloc({ rows: ['5:15'] })"
    ),

    #   first n
    DanfoQuery("select first 4 rows", "%dataframe%.head(4)"),
    DanfoQuery("select first 4 rows and assign to head", "let head = %dataframe%.head(4)"),
    DanfoQuery("select first 7 rows of df", "df.head(7)"),

    #   last n
    DanfoQuery("select last 10 rows", "%dataframe%.tail(10)"),
    DanfoQuery("select last 23 rows of df", "df.tail(23)"),
    DanfoQuery("select last 23 rows of df as tail", "let tail = df.tail(23)"),

    # convert dataframe
    DanfoQuery("convert dataframe to csv", "%dataframe%.toCSV()"),
    DanfoQuery("convert df to csv", "df.toCSV()"),
    DanfoQuery("convert dataframe df to csv as mycsv", "let mycsv = df.toCSV()"),
    DanfoQuery("convert dataframe df to csv", "df.toCSV()"),
    DanfoQuery("convert dataframe to json", "%dataframe%.toJSON()"),

    # append dataframes
    #   using rows (concat([df1, df2]))
    DanfoQuery("append rows of df1 to df2", "danfo.concat({ dfList: [df1, df2], axis: 0 })"),
    DanfoQuery("concat rows of df1 to df2", "danfo.concat({ dfList: [df1, df2], axis: 0 })"),
    DanfoQuery(
        "concat rows of df1 to df2 and assign to result",
        "let result = danfo.concat({ dfList: [df1, df2], axis: 0 })"
    ),
    DanfoQuery("concat df1 and df2", "danfo.concat({ dfList: [df1, df2], axis: 0 })"),
    DanfoQuery("append df1 to df2", "danfo.concat({ dfList: [df1, df2], axis: 0 })"),
    DanfoQuery(
        "append df1 to df2 as result",
        "let result = danfo.concat({ dfList: [df1, df2], axis: 0 })"
    ),

    #   using columns (concat([df1, df2], axis=1))
    DanfoQuery("append columns of df1 to df2", "danfo.concat({ dfList: [df1, df2], axis: 1 })"),
    DanfoQuery("append columns df1 to df2", "danfo.concat({ dfList: [df1, df2], axis: 1 })"),
    DanfoQuery(
        "append columns df1 to df2 as result",
        "let result = danfo.concat({ dfList: [df1, df2], axis: 1 })"
    ),
    DanfoQuery("concat columns of df1 to df2", "danfo.concat({ dfList: [df1, df2], axis: 1 })"),
    DanfoQuery("concat columns df1 and df2", "danfo.concat({ dfList: [df1, df2], axis: 1 })"),
    DanfoQuery(
        "concat columns df1 and df2 and assign to result",
        "let result = danfo.concat({ dfList: [df1, df2], axis: 1 })"
    ),

    # sort dataframe
    DanfoQuery("sort df", "df.sortValues(%col%, { inplace: true })"),
    DanfoQuery("sort df by col", "df.sortValues(col, { inplace: true })"),
    DanfoQuery("sort by col", "%dataframe%.sortValues(col, { inplace: true })"),
    DanfoQuery("sort by 'col'", "%dataframe%.sortValues('col', { inplace: true })"),
    DanfoQuery("sort df by 'col'", "df.sortValues('col', { inplace: true })"),

    # reduce column
    #   into max
    DanfoQuery("select maximum", "%dataframe%.max()"),
    DanfoQuery("select maxima of df", "df.max()"),
    DanfoQuery("select maximum from col in df", "df[col].max()"),
    DanfoQuery("retrieve maximum from col in df", "df[col].max()"),
    DanfoQuery("select maximum from col in df as max", "let max = df[col].max()"),
    DanfoQuery("select maximum from col in df as result", "let result = df[col].max()"),
    DanfoQuery("select maximum in df from col", "df[col].max()"),
    DanfoQuery("get maximum from col in df", "df[col].max()"),

    #   into min
    DanfoQuery("select minimum", "%dataframe%.min()"),
    DanfoQuery("select minima of df", "df.min()"),
    DanfoQuery("get minima of df", "df.min()"),
    DanfoQuery("retrieve minima of df", "df.min()"),
    DanfoQuery("select minimum from col in df", "df[col].min()"),
    DanfoQuery("get minimum from col in df as min", "let min = df[col].min()"),
    DanfoQuery("select minimum from col in df and assign to min", "let min = df[col].min()"),
    DanfoQuery("retrieve minimum from col in df as result", "let result = df[col].min()"),
    DanfoQuery("get minimum from col in df", "df[col].min()"),
    DanfoQuery("select minimum in df from col", "df[col].min()"),

    #   into mean
    DanfoQuery("get average", "%dataframe%.mean()"),
    DanfoQuery("get average as result", "let result = %dataframe%.mean()"),
    DanfoQuery("retrieve average as result", "let result = %dataframe%.mean()"),
    DanfoQuery("retrieve average and assign to result", "let result = %dataframe%.mean()"),
    DanfoQuery("get average from df", "df.mean()"),
    DanfoQuery("select mean from col in df", "df[col].mean()"),
    DanfoQuery("select average from col in df", "df[col].mean()"),
    DanfoQuery("select average from col in df and assign to mean", "let mean = df[col].mean()"),
    DanfoQuery("get average from col in df", "df[col].mean()"),
    DanfoQuery("get mean from col in df", "df[col].mean()"),
]

random.shuffle(manualDanfoQueries)

trainingManualDanfoQueries = \
    manualDanfoQueries[int(len(manualDanfoQueries) * 0.1):]

testingManualDanfoQueries = \
    manualDanfoQueries[:int(len(manualDanfoQueries) * 0.1)]
