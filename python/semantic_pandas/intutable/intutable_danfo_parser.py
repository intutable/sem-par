"""
Utility class for parsing utterances to danfo.js snippets.
Please see `help(IntutableDanfoParser)` for more information.
"""

from typing import Optional, Sequence, Tuple
from math import sqrt

from ..sklearn_parse_scorer import SklearnParseScorer
from ..evaluators.danfo_semantic_evaluator import DanfoSemanticEvaluator
from ..parse import Parse
from ..semantic_parser import SemanticParser
from ..grammars.danfo_grammar import danfoGrammar
from ..utility import loadResource


class IntutableDanfoParser:
    """
    Utility class intended for the intutable project.

    The main purpose of this class is to minimize the amount of Python
    statements that have to be explicitly called from the TypeScript
    intutable plugin.
    """

    def __init__(
        self,
        scalerIdentifier: Optional[str] = None,
        reducerIdentifier: str = "none_featureagg_reducer_md",
        scoringModelIdentifier: str = "none_featureagg_forest_model_md"
    ):
        scaler = None
        if scalerIdentifier is not None:
            scaler = loadResource(scalerIdentifier)

        reducer = loadResource(reducerIdentifier)
        scoringModel = loadResource(scoringModelIdentifier)

        scorer = SklearnParseScorer(
            scoringModel,
            lambda parse: parse.featureVector(danfoGrammar),
            [scaler, reducer] if scaler is not None else [reducer]
        )

        self.evaluator = DanfoSemanticEvaluator()
        self.semanticParser = SemanticParser(danfoGrammar, scorer)

    def parse(self, utterance: str, similarityThreshold: float = 1) -> Optional[Tuple[str, float]]:
        """
        Parse utterance and return the most likely utterance and score.
        """
        scoredSnippets = self.parseAll(utterance, similarityThreshold)

        return None if len(scoredSnippets) == 0 else scoredSnippets[0]

    def parseAll(
        self, utterance: str, similarityThreshold: float = 1
    ) -> Sequence[Tuple[str, float]]:
        """
        Parse utterance and return an ordered list of snippets and scores.

        The corresponding list is ordered by the score of each snippet from high to low.
        """
        scoredParses: Sequence[Tuple[Parse, float]] = self.semanticParser.parseAll(
            utterance, similarityThreshold
        )
        scoredSnippets: Sequence[Tuple[str, float]] = []

        for parse, score in scoredParses:
            snippet = self.evaluator.evaluate(parse)
            scoredSnippets.append((snippet, score))

        return scoredSnippets

    def getSimilarParses(
        self, utterance: str, parse: Parse, similarityThreshold: float = 1
    ) -> Sequence[Tuple[Parse, float]]:
        """
        Parse the utterance and return parses in order of similarity to the given parse.
        """

        # get all parses for utterance
        scoredParses = self.semanticParser.parseAll(utterance, similarityThreshold)

        # get feature vector to compare to
        originalFeatureVector = parse.featureVector(danfoGrammar)

        # determine similarity metric for each parse
        similarityScoredParses = []
        for similarParse, _ in scoredParses:
            featureVector = similarParse.featureVector(danfoGrammar)

            # calculate eucledian distance of vectors as similarity measure
            eucledianDistance = sqrt(sum(
                [
                    (f[0] - f[1]) ** 2
                    for f
                    in zip(originalFeatureVector, featureVector)
                ]
            ))

            similarityScoredParses.append((similarParse, eucledianDistance))

        # order parses by similarity
        similarityScoredParses.sort(key=lambda x: x[1], reverse=True)

        return similarityScoredParses

    def getSimilarSnippets(
        self, utterance: str, snippet: str, similarityThreshold: float = 1
    ) -> Sequence[Tuple[str, float]]:
        """
        Get snippets similar to the given snippet for the given utterance.
        """
        # get parse corresponding to snippet
        scoredParses = self.semanticParser.parseAll(utterance, similarityThreshold)
        parseMatchingUtterance = None
        for parse, _ in scoredParses:
            if snippet == self.evaluator.evaluate(parse):
                parseMatchingUtterance = parse
                break
        else:
            raise ValueError(
                f"Could not find matching parse for utterance = '{utterance}'"
                f"and snippet = '{snippet}'"
            )

        # get similar parses
        smiliarScoredParses = self.getSimilarParses(utterance, parseMatchingUtterance)

        # get snippets from parses
        scoredSnippets = []
        for parse, score in smiliarScoredParses:
            similarSnippet = self.evaluator.evaluate(parse)
            # ignore snippets which are the same as the input snippet
            if similarSnippet != snippet:
                scoredSnippets.append((similarSnippet, score))

        return scoredSnippets
