"""Utility code for interfacing of TypeScript intutable plugin."""

from .intutable_danfo_parser import IntutableDanfoParser

__all__ = [
    "IntutableDanfoParser"
]
