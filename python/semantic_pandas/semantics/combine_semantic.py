"""
Class module for the `CombineSemantic` class.
Please see `help(CombineSemantic)` for more information.
"""

from typing import Any, Dict, Iterable, Sequence

from .semantic import Semantic


class CombineSemantic(Semantic):
    """Semantic for combining the input semantics."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self, positions: Iterable[int], additionals: Dict[str, Any] = None):
        """Please see `help(CombineSemantic)` for more information."""
        if positions is None:
            positions = []

        if additionals is None:
            additionals = []

        self.additionals = additionals
        self.positions = positions

    def compute(self, semantics: Sequence[Dict[str, Any]]) -> Dict[str, Any]:
        """Combine the input semantics by merging the dictionaries."""
        combinedSemantics = {}

        # validate that all positions can be reached in the input semantics
        for pos in self.positions:
            if pos >= len(semantics):
                raise IndexError(f"Index {pos} out of range.")

        if self.additionals is not None:
            combinedSemantics.update(self.additionals)

        for i, semantic in enumerate(semantics):
            if i in self.positions and semantic is not None:
                combinedSemantics.update(semantic)

        return combinedSemantics
