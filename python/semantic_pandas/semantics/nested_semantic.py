"""
Class module for the `NestedSemantic` class.
Please see `help(NestedSemantic)` for more information.
"""

from typing import Any, Dict, Sequence, Mapping

from .semantic import Semantic


class NestedSemantic(Semantic):
    """Semantic for wrapping input semantics in a new dictionary using the given label mapping."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self, labelMapping: Mapping[str, int]):
        self.labelMapping: Mapping[str, int] = labelMapping

    def compute(self, semantics: Sequence[Dict[str, Any]]) -> Dict[str, Any]:
        """Compute the semantics by wrapping the input semantic in the new dictionary."""
        nestedSemantics = {}

        for label, position in self.labelMapping.items():
            nestedSemantics[label] = semantics[position]

        return nestedSemantics
