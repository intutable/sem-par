"""
Class module for the `CustomSemantic` class.
Please see `help(CustomSemantic)` for more information.
"""

from __future__ import annotations  # for self referencing type annotations
from typing import Sequence, Dict, Any, Callable

from .semantic import Semantic


class CustomSemantic(Semantic):
    """A generic semantic which can be used to apply a custom operation to the input semantics."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self, computeFunction: Callable[[Sequence[Dict[str, Any]]], Dict[str, Any]]):
        self.computeFunction = computeFunction

    def compute(self, semantics: Sequence[Dict[str, Any]]) -> Dict[str, Any]:
        """Compute the semantics by applying the given custom operation."""
        return self.computeFunction(semantics)
