"""
Class module for the `Semantic` class.
Please see `help(Semantic)` for more information.
"""

from __future__ import annotations  # for self referencing type annotations
from abc import ABC, abstractmethod
from typing import Sequence, Dict, Any


class Semantic(ABC):
    """Abstract base class for rule semantics."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    @abstractmethod
    def compute(self, semantics: Sequence[Dict[str, Any]]) -> Dict[str, Any]:
        """Compute the semantics using the given input semantics."""
        pass  # pylint: disable=unnecessary-pass  # explicit pass for empty method
