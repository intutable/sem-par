"""
Class module for the `EmptySemantic` class.
Please see `help(EmptySemantic)` for more information.
"""

from .semantic import Semantic


class EmptySemantic(Semantic):
    """
    Semantic that always returns an empty dictionary when computed.

    Usefull for debug purposes or to represent meaningless parts of a NL utterance.
    """
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self):
        """Please see `help(EmptySemantic)` for more information."""
        pass  # pylint: disable=unnecessary-pass  # explicit pass for empty method

    def compute(self, *args, **kwargs) -> dict:
        """Return an empty dictionary."""
        # NOTE: unused arguments are fine since this is a dummy function that intentionally does
        #   nothing
        # pylint: disable=unused-argument
        return {}
