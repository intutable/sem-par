"""
Class module for the `SemanticParser` class.
Please see `help(SemanticParser)` for more information.
"""

from typing import Callable, Sequence, Tuple

from .grammar import Grammar
from .parse import Parse


class SemanticParser:
    """Utility class for parsing natural language utterances."""

    def __init__(
        self,
        grammar: Grammar,
        scoringFunction: Callable[[Sequence[Parse]], float]
    ):
        """Please see `help(Grammar)` for more information."""
        self.grammar = grammar
        self.scoringFunction = scoringFunction

    def parse(self, utterance: str, similarityThreshold: float = 1) -> Tuple[Parse, float]:
        """Parse the given utterance and return the highest scoring parse."""
        return self.parseAll(utterance, similarityThreshold)[0]

    def parseAll(
        self, utterance: str, similarityThreshold: float = 1
    ) -> Sequence[Tuple[Parse, float]]:
        """
        Parse the given utterance and return all possible parses.

        Parses are returned in the order of their corresponding score (descending).
        """
        # generate all possible parses
        parses = self.grammar.parse(utterance, similarityThreshold)

        # score all parses
        scores: float = self.scoringFunction(parses)
        scoredParses: Sequence[Tuple[Parse, float]] = list(zip(parses, scores))

        # sort parses by score (descending)
        scoredParses.sort(key=lambda x: x[1], reverse=True)

        return scoredParses
