"""Script for baking models and training data as package internal resources"""
# NOTE: script could be refactored using smaller functions
# pylint: disable=too-many-locals,too-many-branches,too-many-statements

import sys
import time
from collections import namedtuple
from argparse import ArgumentParser

from scipy.sparse import csr_matrix
from sklearn.preprocessing import StandardScaler, MaxAbsScaler
from sklearn.random_projection import SparseRandomProjection
from sklearn.decomposition import PCA
from sklearn.feature_selection import GenericUnivariateSelect
from sklearn.cluster import FeatureAgglomeration
from sklearn.neural_network import MLPRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor


from ..utility import saveResource, loadResource
from ..datasets.manual_danfo_queries import (
    trainingManualDanfoQueries,
    testingManualDanfoQueries
)
from ..datasets.other_queries import trainingOtherQueries, testingOtherQueries
from ..evaluators import DanfoSemanticEvaluator
from ..grammars import danfoGrammar
from ..utility import trainingVectorsFromDenotations

# configuration
SKIP_EXISTING = True
SKIP_MODEL_SCORING = True
SEED = 4244919083  # use random seed for reproducable results

LabeledModel = namedtuple("ModelLabelPair", ["model", "pair"])


def getScalers():
    """Return the scalers to be baked."""
    return [
        LabeledModel(None, "none"),
        LabeledModel(StandardScaler(), "standard"),
        LabeledModel(MaxAbsScaler(), "maxabs")
    ]


def getReducers(targetFeatures):
    """Return the reducers to be baked."""
    return [
        LabeledModel(
            SparseRandomProjection(
                n_components=targetFeatures,
                random_state=SEED),
            "sparserandom"
        ),
        LabeledModel(
            FeatureAgglomeration(n_clusters=targetFeatures),
            "featureagg"
        ),
        LabeledModel(
            GenericUnivariateSelect(mode='k_best', param=targetFeatures),
            "univariate"
        ),
        LabeledModel(
            PCA(n_components=targetFeatures, random_state=SEED),
            "pca"
        )
    ]


def getModels(targetFeatures):
    """Return the models to be baked."""
    return [
        LabeledModel(
            MLPRegressor(
                hidden_layer_sizes=int((targetFeatures * 0.67)),
                solver="lbfgs",
                random_state=SEED,
                max_iter=10000
            ),
            "perceptron"
        ),
        LabeledModel(
            KNeighborsRegressor(),
            "kneighbors"
        ),
        LabeledModel(
            DecisionTreeRegressor(
                random_state=SEED
            ),
            "tree"
        ),
        LabeledModel(
            SVR(),
            "svr"
        ),
        LabeledModel(
            GradientBoostingRegressor(random_state=SEED),
            "gradientboosting"
        ),
        LabeledModel(
            RandomForestRegressor(random_state=SEED),
            "forest"
        )
    ]


def parseArgs():
    """Parse the input arguments to the script."""
    argumentParser = ArgumentParser()
    argumentParser.add_argument("size_suffix", type=str)
    argumentParser.add_argument("target_features", type=int)

    return argumentParser.parse_args()


def timestampPrint(obj):
    """Print the given object and prefix the current time as timestamp."""
    now = time.localtime()
    timestamp = time.strftime("%H:%m:%S", now)
    print(f"[{timestamp}] {obj}")


def generateTrainingVectors(trainingData):
    """Generate training feature vectors and result vector from intent-snippet-pairs."""
    grammar = danfoGrammar
    evaluator = DanfoSemanticEvaluator()

    trainingInputVectors, trainingOutput = trainingVectorsFromDenotations(
        trainingData,
        lambda data: grammar.parse(data.intent),
        evaluator.evaluate,
        lambda evaluated, data: 1 if evaluated == data.snippet else 0,
        lambda parse: parse.featureVector(grammar)
    )

    return trainingInputVectors, trainingOutput


def generateOtherTrainingVectors(trainingData):
    """Generate training feature vectors and result vector from non-domain specific intents."""
    grammar = danfoGrammar
    trainingInputVectors = []
    trainingOutput = []

    for entry in trainingData:
        parses = grammar.parse(entry)
        for parse in parses:
            domainIsOther = (parse.semantics is not None and
                             parse.semantics.get("domain", None) == "other")

            trainingInputVectors.append(parse.featureVector(grammar))
            trainingOutput += [1 if domainIsOther else 0]

    return trainingInputVectors, trainingOutput


def main():
    """Run the script."""
    args = parseArgs()

    sizeSuffix = args.size_suffix
    targetFeatures = args.target_features

    scalers = getScalers()
    reducers = getReducers(targetFeatures)
    models = getModels(targetFeatures)

    # LOAD TRAININGS DATA
    timestampPrint("Loading datasets...")
    trainingData = trainingManualDanfoQueries
    otherTrainingData = trainingOtherQueries
    testingData = testingManualDanfoQueries
    otherTestingData = testingOtherQueries
    timestampPrint("Done.")

    # CREATE TRAINING VECTORS
    sparseTrainingDataTuple = loadResource("danfo_training_data_sparse")
    if SKIP_EXISTING and sparseTrainingDataTuple is not None:
        timestampPrint("Skipping existing resource 'danfo_training_data_sparse'")

        # unpack from sparse
        trainingFeatureVectors = sparseTrainingDataTuple[0].toarray()
        trainingResultVectors = sparseTrainingDataTuple[1].toarray()[0]
    else:
        timestampPrint("Baking trainings feature vectors...")

        # generate training vectors
        trainingFeatureVectors, trainingResultVectors = \
            generateTrainingVectors(trainingData)

        # generate other trainings vectors
        otherTrainingFeatureVectors, otherTrainingResultVectors = \
            generateOtherTrainingVectors(otherTrainingData)

        # combine datasets
        trainingFeatureVectors = trainingFeatureVectors + otherTrainingFeatureVectors
        trainingResultVectors = trainingResultVectors + otherTrainingResultVectors

        # make trainings data sparse for persisting purposes
        sparseTrainingsDataTuple = (
            csr_matrix(trainingFeatureVectors),
            csr_matrix(trainingResultVectors)
        )

        saveResource(sparseTrainingsDataTuple, "danfo_training_data_sparse")

        timestampPrint("Done.")

    # CREATE TESTING VECTORS
    sparseTestingDataTuple = loadResource("danfo_testing_data_sparse")
    if SKIP_EXISTING and sparseTestingDataTuple is not None:
        timestampPrint("Skipping existing resource 'danfo_testing_data_sparse'")

        # NOTE: no need to load them, they are not required here
    else:
        timestampPrint("Baking testing feature vectors...")

        # generate training vectors
        testingFeatureVectors, testingResultVectors = \
            generateTrainingVectors(testingData)

        # generate trainings vectors
        otherTestingFeatureVectors, otherTestingResultVectors = \
            generateOtherTrainingVectors(otherTestingData)

        # combine datasets
        testingFeatureVectors = testingFeatureVectors + otherTestingFeatureVectors
        testingResultVectors = testingResultVectors + otherTestingResultVectors

        # make trainings data sparse for persisting purposes
        sparseTestingDataTuple = (
            csr_matrix(testingFeatureVectors),
            csr_matrix(testingResultVectors)
        )
        saveResource(sparseTestingDataTuple, "danfo_testing_data_sparse")

        timestampPrint("Done.")

    # TRAING AND SAVE SCALERS
    timestampPrint("Baking scalers...")

    for scaler, label in scalers:
        resourceName = f"{label}_scaler"
        if SKIP_EXISTING and loadResource(resourceName) is not None:
            timestampPrint(f"Skipping existing resource '{resourceName}'")
        else:
            timestampPrint(f"Baking resource '{resourceName}'...")

            if scaler is not None:
                scaler.fit(trainingFeatureVectors, trainingResultVectors)
                saveResource(scaler, resourceName)

            timestampPrint("Done.")

    # TRAIN AND SAVE REDUCERS
    timestampPrint("Baking reducers...")

    for _, scalerLabel in scalers:
        scaler = loadResource(f"{scalerLabel}_scaler")
        if scaler is not None:
            scaledTrainingFeatureVectors = scaler.transform(trainingFeatureVectors)
        else:
            scaledTrainingFeatureVectors = trainingFeatureVectors

        for reducer, label in reducers:
            resourceName = f"{scalerLabel}_{label}_reducer_{sizeSuffix}"

            if SKIP_EXISTING and loadResource(resourceName) is not None:
                timestampPrint(f"Skipping existing resource '{resourceName}'")
            else:
                timestampPrint(f"Baking resource '{resourceName}'...")

                reducer.fit(scaledTrainingFeatureVectors, trainingResultVectors)
                saveResource(reducer, resourceName)

                timestampPrint("Done.")

    # TRAIN AND SAVE SCORING MODELS
    timestampPrint("Baking models...")

    for _, scalerLabel in scalers:
        scaler = loadResource(f"{scalerLabel}_scaler")

        for _, reducerLabel in reducers:
            reducer = loadResource(f"{scalerLabel}_{reducerLabel}_reducer_{sizeSuffix}")

            if scaler is not None:
                scaledTrainingFeatureVectors = scaler.transform(trainingFeatureVectors)
            else:
                scaledTrainingFeatureVectors = trainingFeatureVectors

            reducedTrainingFeatureVectors = reducer.transform(scaledTrainingFeatureVectors)

            for model, label in models:
                resourceName = f"{scalerLabel}_{reducerLabel}_{label}_model_{sizeSuffix}"

                if SKIP_EXISTING and loadResource(resourceName) is not None:
                    timestampPrint(f"Skipping existing resource '{resourceName}'")
                else:
                    timestampPrint(f"Baking resource '{resourceName}'...")

                    model.fit(reducedTrainingFeatureVectors, trainingResultVectors)
                    saveResource(model, resourceName)

                    timestampPrint("Done.")

                    if not SKIP_MODEL_SCORING:
                        timestampPrint("Scoring model...")
                        trainingScore = model.score(
                            reducedTrainingFeatureVectors,
                            trainingResultVectors
                        )
                        timestampPrint(f"Training Accuracy = {trainingScore}.")


if __name__ == "__main__":
    sys.exit(main())
