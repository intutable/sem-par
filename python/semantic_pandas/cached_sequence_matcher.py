"""
Class module for the `CachedSequenceMatcher` class.
Please see `help(CachedSequenceMatcher)` for more information.
"""

from functools import cached_property, lru_cache
from typing import Sequence, Tuple

import spacy

from .utility import makeLruCached


class CachedSequenceMatcher:
    """
    Utility class for comparing string sequences based on their wordembeddings.

    This class uses internal caches to enhance performance.
    """

    # Spacy models can be quite large, only cache the most recent one
    # since we do not intend to mix different models
    @staticmethod  # method is static to share models among instances
    @lru_cache(maxsize=1)
    def loadSpacyModelCached(name: str):
        """
        Load spacy model and cache result for future calls.

        This function is mostly used to only load the spacy model when
        we need it.
        """
        return spacy.load(name)

    def __init__(
        self,
        sequenceCacheSize: int,
        tokenCacheSize: int,
        spacyModelName: str = "en_core_web_md"
    ):
        # Create cached versions of member functions with according cache sizes
        self._similarCached = makeLruCached(self._similar, sequenceCacheSize)
        self._getTokensCached = makeLruCached(self._getTokens, tokenCacheSize)

        self.spacyModelName = spacyModelName

    def similar(
        self,
        sequence1: Sequence[str],
        sequence2: Sequence[str],
        similarityThreshold: float
    ) -> bool:
        """
        Determine wether the given sequences are similar based on word embeddings.

        Note that 'similarity' indicates that the words are synonymous rather than that
        they have a short editing distance.
        """
        # NOTE: make tuples from sequences to ensure that they are hashable for
        # the LRU cache
        return self._similarCached(tuple(sequence1), tuple(sequence2), similarityThreshold)

    def getTokens(self, string: str):
        """Get spacy tokens of given string."""
        return self._getTokensCached(string)

    def _similar(
        self,
        sequence1: Tuple[str],
        sequence2: Tuple[str],
        similarityThreshold: float
    ) -> bool:
        if len(sequence1) == 0 and len(sequence2) == 0:
            return True

        if len(sequence1) == 0 and len(sequence2) != 0:
            return False

        if len(sequence1) != 0 and len(sequence2) == 0:
            return False

        seqStr1 = " ".join(sequence1)
        seqStr2 = " ".join(sequence2)

        tokens1 = self.getTokens(seqStr1)
        tokens2 = self.getTokens(seqStr2)

        return tokens1.similarity(tokens2) >= similarityThreshold

    def _getTokens(self, string: str):
        return self.spacyModel(string)

    @cached_property
    def spacyModel(self):
        """The internal spacy model used for word embeddings"""
        return CachedSequenceMatcher.loadSpacyModelCached(self.spacyModelName)
