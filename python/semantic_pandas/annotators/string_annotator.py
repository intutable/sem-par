"""
Class module for the `StringAnnotator` class.
Please see `help(StringAnnotator)` for more information.
"""

from typing import Sequence, Dict, Any

from .annotator import Annotator


class StringAnnotator(Annotator):
    """Annotator for annotating simple strings."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self, delimiter: str = "'"):
        self.delimiter = delimiter

    def annotate(self, tokens: Sequence[str]) -> Dict[str, Any]:
        """
        Return a dictionary representing the semantics of the annotated tokens.

        The returned semantics are a dictionary of the shape:
        {'string': str}
        """
        annotation = None
        if tokens[0].startswith(self.delimiter) and tokens[-1].endswith(self.delimiter):
            # string = " ".join(tokens)[1:-1]  # NOTE: remove leading and trailing delimiter
            string = " ".join(tokens)
            annotation = {"string": string}

        return annotation
