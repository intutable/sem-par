"""
Class module for the `SeparatedTokenListAnnotator` class.
Please see `help(SeparatedTokenListAnnotator)` for more information.
"""

from typing import Sequence, Dict, Any

from .annotator import Annotator


class SeparatedTokenListAnnotator(Annotator):
    """
    Annotator for annotating separated lists of tokens.

    Example of separated list: 'blue, green, yellow'
    """
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self, separator: str = ","):
        """Please see `help(SeparatedTokenListAnnotator)` for more information."""
        self.separator = separator

    def annotate(self, tokens: Sequence[str]) -> Dict[str, Any]:
        """
        Return a dictionary representing the semantics of the annotated tokens.

        The returned semantics are a dictionary of the shape:
        {'tokenList': Sequence[str]}
        """
        annotation = None

        if (len(tokens) > 1 and
                all(token.endswith(self.separator) for token in tokens[:-1]) and
                not tokens[-1].endswith(self.separator)):
            tokenList = [token[:-1] for token in tokens[:-1]] + [tokens[-1]]
            annotation = {"tokenList": tokenList}

        return annotation
