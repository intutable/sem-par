"""
Class module for the `Grammar` class.
Please see `help(Grammar)` for more information.
"""

from typing import Sequence, Iterable, Dict, Union, Tuple
from collections import defaultdict
from itertools import product
from functools import cached_property
from copy import copy

from .rules import Rule
from .parse import Parse
from .utility import isOptional, cellIsFull, deoptionalize
from .rules.lexical_rule import LexicalRule
from .rules.unary_rule import UnaryRule
from .rules.binary_rule import BinaryRule
from .rules.n_ary_rule import NAryRule
from .rules.annotator_rule import AnnotatorRule
from .rules.functions import makeRule
from .semantics.custom_semantic import CustomSemantic
from .cached_sequence_matcher import CachedSequenceMatcher


class Grammar:
    """Represents a combinatory categorial grammar for grammar based semantic parsing."""
    def __init__(
            self,
            rules: Iterable[Rule] = None,
            startSymbol: str = None):
        """Please see `help(Grammar)` for more information."""

        # Construction of sequence matcher is deferred to when we
        # actually need it (see corresponding property)
        self._sequenceMatcher = None

        self.lexicalRules: Dict[str, Sequence[LexicalRule]] = {}
        self.unaryRules: Dict[str, Sequence[UnaryRule]] = {}
        self.binaryRules: Dict[str, Sequence[BinaryRule]] = {}
        self.annotatorRules: Sequence[AnnotatorRule] = []
        self.startSymbol = startSymbol

        rule = rules if rules is not None else []
        for rule in rules:
            self._addRule(rule)

    def _addRule(self, rule: Rule) -> None:
        if rule.containsOptional():  # rule with optionals
            self._addRuleContainingOptional(rule)
        elif isinstance(rule, LexicalRule):  # lexical rule
            self._sequenceMatcher = None  # invalidate sequence matcher
            self.lexicalRules.setdefault(rule.rhs, []).append(rule)
        elif isinstance(rule, UnaryRule):  # unary rule
            self.unaryRules.setdefault(rule.rhs, []).append(rule)
        elif isinstance(rule, BinaryRule):  # binary rule
            self.binaryRules.setdefault(rule.rhs, []).append(rule)
        elif isinstance(rule, NAryRule):  # n-ary rule
            lhsRule, rhsRule = rule.binarize()
            self._addRule(lhsRule)
            self._addRule(rhsRule)
        elif isinstance(rule, AnnotatorRule):  # annotator rule
            self.annotatorRules.append(rule)
        else:  # unknown rule type
            raise Exception(f"Unknown rule type: {rule}")

    # Function has a lot of arguments, but creating a level of abstraction here is most likely
    # too much effort for too little gain and will not be much simpler than plain function
    # arguments
    # pylint: disable=too-many-arguments
    def _applyLexicalRules(
        self, chart: dict, tokens: Sequence[str], i: int, j: int, similarityThreshold: float = 1
    ):
        tokenRange = tokens[i:j]
        # NOTE: do not perform similarity matching if threshold is larger than 1 to
        # increase performance
        if similarityThreshold >= 1:
            # apply all rules that match the RHS exactly
            for rule in self.lexicalRules.get(tuple(tokenRange), []):
                chart[(i, j)].append(Parse(rule, tokenRange))
        else:
            # apply all rules with a similar RHS
            for ruleString, rules in self.lexicalRules.items():
                if self.sequenceMatcher.similar(ruleString, tokenRange, similarityThreshold):
                    for rule in rules:
                        chart[(i, j)].append(Parse(rule, tokenRange))

    def _applyUnaryRules(self, chart: dict, i: int, j: int):
        """Add parses to chart cell (i, j) by applying unary rules."""
        # Note that the last line of this method can add new parses to chart[(i,
        # j)], the list over which we are iterating.  Because of this, we
        # essentially get unary closure "for free".  (However, if the grammar
        # contains unary cycles, we'll get stuck in a loop, which is one reason for
        # check_capacity().)
        for parse in chart[(i, j)]:
            for rule in self.unaryRules.get((parse.rule.lhs,), []):
                if cellIsFull(chart, i, j):
                    break
                chart[(i, j)].append(Parse(rule, [parse]))

    def _applyBinaryRules(self, chart: dict, i: int, j: int):
        """Add parses to span (i, j) in chart by applying binary rules from grammar."""
        for k in range(i + 1, j):  # all ways of splitting the span into two subspans
            for parse1, parse2 in product(chart[(i, k)], chart[(k, j)]):
                for rule in self.binaryRules.get((parse1.rule.lhs, parse2.rule.lhs), []):
                    chart[(i, j)].append(Parse(rule, [parse1, parse2]))

    def _applyAnnotators(self, chart: dict, tokens: Sequence[str], i: int, j: int):
        """Add parses to chart cell (i, j) by applying annotators."""
        for rule in self.annotatorRules:
            if not cellIsFull(chart, i, j):
                if rule.isValid(tokens[i:j]):
                    chart[(i, j)].append(Parse(rule, tokens[i:j]))

    def _addRuleContainingOptional(self, rule: Rule):
        if len(rule.rhs) <= 1:
            raise Exception(f"Entire RHS of rule is optional: {rule}")

        # Find index of the first optional element on the RHS.
        first = next((i for i, label in enumerate(rule.rhs) if isOptional(label)), -1)
        if first < 0:
            raise Exception(f"Rule does not contain optional: {rule}")

        prefix = rule.rhs[:first]
        suffix = rule.rhs[(first + 1):]

        # First variant: the first optional element gets deoptionalized.
        deoptionalized = tuple([deoptionalize(rule.rhs[first])])
        self._addRule(makeRule(rule.lhs, prefix + deoptionalized + suffix, rule.semantic))

        # Second variant: the first optional element gets removed.
        # supply a dummy argument for the removed element.
        def makeOptionalSemantics(semantics):
            optionalSemantics = None
            if semantics is not None:
                optionalSemantics = semantics[:first] + [None] + semantics[first:]

            return optionalSemantics

        semantic = CustomSemantic(
            lambda semantics: rule.semantic.compute(makeOptionalSemantics(semantics))
        )
        self._addRule(makeRule(rule.lhs, prefix + suffix, semantic))

    def parse(self, sentence: str, similarityThreshold: float = 1) -> Sequence[Parse]:
        """Returns a list of parses for the given sentence."""
        tokens = sentence.split()
        chart = defaultdict(list)  # map from span (start, end) to list of parses

        # iterate over all possible token spans in order of length
        for length in range(1, len(tokens) + 1):
            for start in range(len(tokens) - length + 1):
                end = start + length

                self._applyAnnotators(chart, tokens, start, end)
                self._applyLexicalRules(chart, tokens, start, end, similarityThreshold)

                self._applyBinaryRules(chart, start, end)
                self._applyUnaryRules(chart, start, end)

        parses = chart[(0, len(tokens))]

        # remove parses that do not have the expected start symbol as semantic root (if defined)
        if self.startSymbol is not None:
            parses = [parse for parse in parses if parse.rule.lhs == self.startSymbol]

        return parses

    @property
    def sequenceMatcher(self) -> CachedSequenceMatcher:
        """CachedSequenceMatcher for comparing sequences internally."""
        if self._sequenceMatcher is None:
            # Difficult to determine an accurate reqiured size for the sequence
            # cache, but len(self.lexicalRules) squared usually works pretty well
            sequenceCacheSize = len(self.lexicalRules) * 128
            tokenCacheSize = len(self.lexicalRules) + 128
            self._sequenceMatcher = CachedSequenceMatcher(
                sequenceCacheSize, tokenCacheSize, "en_core_web_md"
            )

        return self._sequenceMatcher

    @cached_property
    def _rules(self) -> Sequence[Rule]:
        """A list of all rules contained by this grammar."""
        return [rule for rules in self.lexicalRules.values() for rule in rules] + \
            [rule for rules in self.unaryRules.values() for rule in rules] + \
            [rule for rules in self.binaryRules.values() for rule in rules] + \
            self.annotatorRules

    @property
    def rules(self) -> Sequence[Rule]:
        """
        A list of all rules contained in the grammar.

        Note that changes to this list do not add/remove rules from this grammar.
        """
        # NOTE: do not return the cached property itself, since the returned object may be
        # manipulated by the caller, resulting in inaccurate values for later calls
        return copy(self._rules)

    @cached_property
    def _ruleFeatureDict(self) -> Dict[Union[str, Tuple[str, str]], float]:
        ruleFeatureDict = {}

        # single rules
        for rule in self.rules:
            ruleFeatureDict[str(rule)] = 0

        # combined rules (aka rule precedences)
        for rule1 in self.rules:
            for rule2 in self.rules:
                # Only create precedence entries for rules
                # that may actually preceed each other
                if rule2.lhs in rule1.rhs:
                    ruleFeatureDict[(str(rule1), str(rule2))] = 0

        return ruleFeatureDict

    @property
    def ruleFeatureDict(self) -> Dict[Union[str, Tuple[str, str]], float]:
        """
        A dictionary mapping string representation of rules to floats with value 0.

        This dictionary is intended to be used for the generation of feature vectors for parse
        scoring. It maps the string representation of single rules as well as tuples of
        the string combinations of binary combinations of rules to floats (which are
        initially set to 0).
        Note that this property returns a new copy of the dictionary for every call.
        """
        # NOTE: do not return the cached property itself, since the returned object may be
        # manipulated by the caller, resulting in inaccurate values for later calls
        return copy(self._ruleFeatureDict)
