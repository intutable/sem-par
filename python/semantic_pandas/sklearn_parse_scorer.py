"""
Class module for the `SklearnParseScorer` class.
Please see `help(SklearnParseScorer)` for more information.
"""


from typing import Callable, Sequence

from .parse import Parse


class SklearnParseScorer:
    """Parse scorer using sklearn models for parsing."""

    def __init__(
        self,
        model,
        toFeatureVector: Callable[[Parse], Sequence[float]],
        transformModels=None
    ):
        """Please see `help(SklearnParseScorer)` for more information."""
        self.model = model
        self.toFeatureVector = toFeatureVector
        self.transformModels = [] if transformModels is None else transformModels

    def __call__(self, parses: Sequence[Parse]) -> Sequence[float]:
        """Score all parses."""
        # Get feature vectors from parses
        featureVectors = [self.toFeatureVector(p) for p in parses]

        # Transform all feature vectors according to transform models
        featureVectors = self.transformFeatureVectors(featureVectors)

        # Get scores from prediction model
        return self.model.predict(featureVectors)

    def transformFeatureVectors(
        self,
        featureVectors: Sequence[Sequence[float]]
    ) -> Sequence[Sequence[float]]:
        """Transform a sequence of feature vectors by applying the transform models in order."""
        for transformModel in self.transformModels:
            featureVectors = transformModel.transform(featureVectors)

        return featureVectors

    def train(
        self,
        featureVectors: Sequence[Sequence[float]],
        resultVector: Sequence[float]
    ) -> float:
        """Train scoring and transform models using the given training data."""

        # train transform models and transform feature vectors for subsequent training
        for transformModel in self.transformModels:
            featureVectors = transformModel.fit_transform(featureVectors)

        # train scoring model
        self.model.fit(featureVectors, resultVector)

        # return final score of training data set
        return self.model.score(featureVectors, resultVector)
