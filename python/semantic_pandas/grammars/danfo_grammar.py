"""Module containing the definition of the grammar used for parsing danfo.js domain utterances."""

from ..rules import makeRule, AnnotatorRule
from ..semantics import (
    ProxySemantic,
    CombineSemantic,
    SimpleSemantic,
    NestedCombineSemantic,
    NestedSemantic,
    CustomSemantic
)
from ..annotators import (
    TokenAnnotator,
    StringAnnotator,
    SimpleNumberAnnotator,
    SeparatedTokenListAnnotator
)
from ..grammar import Grammar

danfoRules = [
    # general rules
    makeRule("$ROOT", "$DanfoQuery", ProxySemantic(0)),
    makeRule("$ROOT", "?$Text $DanfoQuery ?$Text", ProxySemantic(1)),
    makeRule(
        "$DanfoQuery",
        "$OperationWithArguments",
        CombineSemantic([0], additionals={"domain": "danfo"})
    ),
    makeRule(
        "$OperationWithArguments",
        "$Operation $OperationArgument",
        CombineSemantic([0, 1])
    ),
    makeRule(
        "$OperationWithArguments",
        "$OperationArgument $Operation",
        CombineSemantic([0, 1])
    ),
    makeRule(
        "$OperationWithArguments",
        "$OperationWithArguments $OperationArgument",
        CombineSemantic([0, 1])
    ),
    makeRule(
        "$OperationWithArguments",
        "$OperationArgument $OperationWithArguments",
        CombineSemantic([0, 1])
    ),

    # operation rules
    makeRule("$Operation", "$AppendOperation", ProxySemantic(0)),
    makeRule("$Operation", "$CreateOperation", ProxySemantic(0)),
    makeRule("$Operation", "$SelectOperation", ProxySemantic(0)),
    makeRule("$Operation", "$ConvertOperation", ProxySemantic(0)),
    makeRule("$Operation", "$SortOperation", ProxySemantic(0)),
    makeRule("$Operation", "$ReadOperation", ProxySemantic(0)),
    makeRule("$Operation", "$GroupOperation", ProxySemantic(0)),

    #   append operation
    makeRule("$AppendOperation", "$Append", SimpleSemantic({"operation": "append"})),

    #   create operation
    makeRule("$CreateOperation", "$Create", SimpleSemantic({"operation": "create"})),

    #   select operation
    makeRule("$SelectOperation", "$Select", SimpleSemantic({"operation": "select"})),

    #   convert operation
    makeRule("$ConvertOperation", "$Convert", SimpleSemantic({"operation": "convert"})),

    #   sort operation
    makeRule("$SortOperation", "$Sort", SimpleSemantic({"operation": "sort"})),

    #   group operation
    makeRule("$GroupOperation", "$Group", SimpleSemantic({"operation": "group"})),

    #   read operation (read a file from a source)
    makeRule("$ReadOperation", "$Read", SimpleSemantic({"operation": "read"})),

    # operation argument rules
    makeRule("$OperationArgument", "$TypeArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$NamedTypeArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$SelectTypeArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$FromVariableArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$SelectSourceArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$IndexArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$LogicalConditionArgument", NestedSemantic({"condition": 0})),
    makeRule("$OperationArgument", "$RangeArgument", NestedSemantic({"range": 0})),
    makeRule("$OperationArgument", "$OpenRangeArgument", NestedSemantic({"range": 0})),
    makeRule("$OperationArgument", "$HeadEntriesArgument", NestedSemantic({"head": 0})),
    makeRule("$OperationArgument", "$TailEntriesArgument", NestedSemantic({"tail": 0})),
    makeRule("$OperationArgument", "$ConvertFormatArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$ConvertOrderArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$ReadFormatArgument", ProxySemantic((0))),
    makeRule("$OperationArgument", "$AssignArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$AppendTypeArgument", ProxySemantic(0)),
    makeRule(
        "$OperationArgument",
        "$AppendSourceTargetArgument",
        NestedSemantic({"appendSourceTarget": 0})
    ),
    makeRule("$OperationArgument", "$ByArgument", ProxySemantic(0)),
    makeRule("$OperationArgument", "$CombinedSelectSourceArgument", ProxySemantic(0)),


    #   type argument
    makeRule("$TypeArgument", "$Dataframe", SimpleSemantic({"type": "DataFrame"})),
    # makeRule("$TypeArgument", "$Series", SimpleSemantic({"type": "Series"})),

    #   named type argument
    makeRule(
        "$NamedTypeArgument",
        # NOTE: making the "TypeArgument" non optional drastically reduces the amount of possible
        # parses
        "?$TypeArgument $Variable",
        NestedCombineSemantic({"typeVariable": 1}, [0])
    ),

    #   select type argument
    makeRule("$SelectTypeArgument", "$Row", SimpleSemantic({"selectType": "row"})),
    makeRule(
        "$SelectTypeArgument",
        "$Column ?$Symbol",
        NestedCombineSemantic({"columnName": 1}, [], additionals={"selectType": "column"})
    ),
    makeRule("$SelectTypeArgument", "$Maximum ?$Value", SimpleSemantic({"selectType": "maximum"})),
    makeRule("$SelectTypeArgument", "$Minimum ?$Value", SimpleSemantic({"selectType": "minimum"})),
    makeRule("$SelectTypeArgument", "$Average ?$Value", SimpleSemantic({"selectType": "average"})),

    #   select source argument
    makeRule(
        "$SelectSourceArgument",
        "$From $NamedTypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$From $TypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$Of $NamedTypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$Of $TypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$In $NamedTypeArgument",
        NestedSemantic({"selectSource": 1})
    ),
    makeRule(
        "$SelectSourceArgument",
        "$In $TypeArgument",
        NestedSemantic({"selectSource": 1})
    ),

    #   sub select source argument
    makeRule(
        "$SubSelectSourceArgument",
        "$SelectSourceArgument",
        CustomSemantic(lambda sems: {"subSelectSource": sems[0]["selectSource"]})
    ),

    #   combined select source argument
    makeRule(
        "$CombinedSelectSourceArgument",
        "$SubSelectSourceArgument $SelectSourceArgument",
        CombineSemantic([0, 1])
    ),
    makeRule(
        "$CombinedSelectSourceArgument",
        "$SelectSourceArgument $SubSelectSourceArgument",
        CombineSemantic([0, 1])
    ),

    #   from variable argument
    makeRule("$FromVariableArgument", "$From $Variable", NestedSemantic({"from": 1})),

    #   index argument
    makeRule("$IndexArgument", "$Using $Index ?$Token", NestedSemantic({"index": 2})),

    #   range argument
    makeRule("$RangeArgument", "?$From $Number $To $Number", NestedSemantic({"from": 1, "to": 3})),

    #   head/tail entries argument
    makeRule("$HeadEntriesArgument", "$First $Number", ProxySemantic(1)),
    makeRule("$TailEntriesArgument", "$Last $Number", ProxySemantic(1)),

    #   logical condition argument
    makeRule(
        "$LogicalConditionArgument",
        "$Where $Variable $ComparisonOperator ?$Than $Variable",
        NestedCombineSemantic({"lhs": 1, "rhs": 4}, [2])
    ),
    makeRule(
        "$LogicalConditionArgument",
        "$Where $Variable $ComparisonOperator ?$To $Variable",
        NestedCombineSemantic({"lhs": 1, "rhs": 4}, [2])
    ),

    #   convert format argument
    makeRule("$ConvertFormatArgument", "$To $Format", NestedSemantic({"convertFormat": 1})),

    #   convert format order argument
    makeRule(
        "$ConvertOrderArgument",
        "$As $RowMajor",
        SimpleSemantic({"convertOrder": "rowMajor"}),
    ),

    #   read format argument
    makeRule("$ReadFormatArgument", "$Format", NestedSemantic({"readFormat": 0})),

    #   assign argument
    makeRule("$AssignArgument", "$As $Variable", NestedSemantic({"assignTo": 1})),
    makeRule("$AssignArgument", "?$And $Assign $To $Variable", NestedSemantic({"assignTo": 3})),

    #   append type argument
    makeRule("$AppendTypeArgument", "$Row", SimpleSemantic({"appendType": "row"})),
    makeRule("$AppendTypeArgument", "$Column", SimpleSemantic({"appendType": "column"})),

    #   append targets argument
    makeRule(
        "$AppendSourceTargetArgument",
        "?$Of $Variable $To $Variable",
        NestedSemantic({"source": 1, "target": 3})
    ),
    makeRule(
        "$AppendSourceTargetArgument",
        "?$Of $Variable $And $Variable",
        NestedSemantic({"source": 1, "target": 3})
    ),

    #   order by argument
    makeRule(
        "$ByArgument",
        "$By $Symbol",
        NestedSemantic({"by": 1})
    ),
    makeRule(
        "$ByArgument",
        "$Using $Symbol",
        NestedSemantic({"by": 1})
    ),

    # logical operator rules
    makeRule("$ComparisonOperator", "$LogicalGreater", SimpleSemantic({"operator": ">"})),
    makeRule("$ComparisonOperator", "$LogicalGreaterEqual", SimpleSemantic({"operator": ">="})),
    makeRule("$ComparisonOperator", "$LogicalLess", SimpleSemantic({"operator": "<"})),
    makeRule("$ComparisonOperator", "$LogicalLessEqual", SimpleSemantic({"operator": "<="})),
    makeRule("$ComparisonOperator", "$LogicalEqual", SimpleSemantic({"operator": "=="})),
    makeRule("$ComparisonOperator", "$LogicalNotEqual", SimpleSemantic({"operator": "!="})),


    # misc terminal rules
    makeRule("$Append", "append"),
    makeRule("$Append", "concat"),
    makeRule("$Append", "attach"),

    makeRule("$Create", "create"),
    makeRule("$Create", "new"),

    makeRule("$Convert", "convert"),
    makeRule("$Convert", "transform"),
    makeRule("$Convert", "cast"),

    makeRule("$Select", "select"),
    makeRule("$Select", "retrieve"),
    makeRule("$Select", "get"),

    makeRule("$Group", "group"),

    makeRule("$Read", "load"),
    makeRule("$Read", "get"),
    makeRule("$Read", "parse"),
    makeRule("$Read", "read"),
    makeRule("$Read", "download"),

    makeRule("$Sort", "sort"),
    makeRule("$Sort", "order"),

    makeRule("$Csv", "csv"),

    makeRule("$Dict", "dict"),

    makeRule("$Json", "json"),
    makeRule("$Json", "JSON"),

    makeRule("$Excel", "excel"),
    makeRule("$Excel", "xls"),
    makeRule("$Excel", "xlsx"),

    makeRule("$Numpy", "numpy"),

    makeRule("$Where", "where"),

    makeRule("$From", "from"),

    makeRule("$To", "to"),
    makeRule("$To", "into"),

    makeRule("$By", "by"),

    makeRule("$And", "and"),

    makeRule("$Or", "or"),

    makeRule("$On", "on"),

    makeRule("$Is", "is"),

    makeRule("$Of", "of"),

    makeRule("$In", "in"),

    makeRule("$As", "as"),

    makeRule("$Than", "than"),

    makeRule("$Assign", "assign"),

    makeRule("$Using", "using"),
    makeRule("$Using", "with"),
    makeRule("$Using", "$Based $On"),

    makeRule("$Based", "based"),

    makeRule("$Dataframe", "dataframe"),
    # makeRule("$Series", "series"),

    makeRule("$Row", "row"),
    makeRule("$Row", "rows"),
    makeRule("$Row", "entry"),
    makeRule("$Row", "entries"),

    makeRule("$Column", "column"),
    makeRule("$Column", "columns"),

    makeRule("$Index", "index"),

    makeRule("$Value", "value"),
    makeRule("$Value", "values"),

    makeRule("$Biggest", "biggest"),

    makeRule("$Smallest", "smallest"),

    makeRule("$Maximum", "maximum"),
    makeRule("$Maximum", "maxima"),
    makeRule("$Maximum", "$Biggest"),

    makeRule("$Minimum", "minimum"),
    makeRule("$Minimum", "minima"),
    makeRule("$Minimum", "$Smallest"),

    makeRule("$Average", "average"),
    makeRule("$Average", "mean"),

    makeRule("$First", "first"),
    makeRule("$First", "top"),

    makeRule("$Last", "last"),
    makeRule("$Last", "bottom"),

    makeRule("$Not", "not"),

    makeRule("$LogicalGreater", "greater"),
    makeRule("$LogicalGreater", "is greater"),
    makeRule("$LogicalGreater", ">"),

    makeRule("$LogicalLess", "less"),
    makeRule("$LogicalLess", "is less"),
    makeRule("$LogicalLess", "<"),

    makeRule("$LogicalEqual", "equal"),
    makeRule("$LogicalEqual", "is equal"),
    makeRule("$LogicalEqual", "=="),

    makeRule("$LogicalGreaterEqual", "$LogicalGreater ?$Or $LogicalEqual"),
    makeRule("$LogicalGreaterEqual", ">="),

    makeRule("$LogicalLessEqual", "$LogicalLess ?$Or $LogicalEqual"),
    makeRule("$LogicalLessEqual", "<="),

    makeRule("$LogicalNotEqual", "$Not $LogicalEqual"),
    makeRule("$LogicalNotEqual", "!="),

    makeRule("$RowMajor", "row-major"),

    # format rules
    makeRule("$Format", "$Csv", SimpleSemantic({"format": "csv"})),
    makeRule("$Format", "$Dict", SimpleSemantic({"format": "dict"})),
    makeRule("$Format", "$Json", SimpleSemantic({"format": "json"})),
    makeRule("$Format", "$Numpy", SimpleSemantic({"format": "numpy"})),
    makeRule("$Format", "$Excel", SimpleSemantic({"format": "excel"})),

    # variables and literals together form the class of symbols
    makeRule("$Symbol", "$Variable", ProxySemantic(0)),
    makeRule("$Symbol", "$Literal", ProxySemantic(0)),

    # variables rules
    makeRule(
        "$Variable",
        "$Token",
        CustomSemantic(lambda sems: {"variable": sems[0]["token"]})
    ),
    makeRule(
        "$Variable",
        "$String",
        CustomSemantic(lambda sems: {"variable": sems[0]["string"]})
    ),
    makeRule(
        "$Variable",
        "$TokenList",
        CustomSemantic(lambda sems: {"variable": sems[0]["tokenList"]})
    ),

    # literal rules
    makeRule(
        "$Literal",
        "$Token",
        CustomSemantic(lambda sems: {"literal": sems[0]["token"]})
    ),
    makeRule(
        "$Literal",
        "$TokenList",
        CustomSemantic(lambda sems: {"literal": sems[0]["tokenList"]})
    ),

    # annotator rules
    AnnotatorRule("$TextToken", TokenAnnotator()),
    AnnotatorRule("$Token", TokenAnnotator()),
    AnnotatorRule("$TokenList", SeparatedTokenListAnnotator()),
    AnnotatorRule("$String", StringAnnotator()),
    AnnotatorRule("$Number", SimpleNumberAnnotator()),

    # "other" domain rules
    makeRule("$ROOT", "$OtherQuery", ProxySemantic(0)),
    makeRule("$OtherQuery", "$Text", SimpleSemantic({"domain": "other"})),
    makeRule("$Text", "$TextToken ?$Text"),
]

danfoGrammar = Grammar(danfoRules, startSymbol="$ROOT")
