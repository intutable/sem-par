"""Module containing miscallenous, free standing utility functions."""

import os
import pickle
from functools import lru_cache
from typing import Any

MAX_CELL_CAPACITY = 10000
CATEGORY_PREFIX = "$"
OPTIONAL_PREFIX = "?"


def decategorize(token: str):
    """Remove the category prefix from the given token."""

    try:
        prefixPos = token.index(CATEGORY_PREFIX)
    except ValueError:
        # NOTE: raise is not an re-raise, but an error with a different message
        # pylint: disable=raise-missing-from
        raise ValueError(f"Category prefix ({CATEGORY_PREFIX}) was not found in token.")
    return token[prefixPos + 1:]


def deoptionalize(token: str):
    """Remove the optional prefix from the given token."""

    try:
        prefixPos = token.index(OPTIONAL_PREFIX)
    except ValueError:
        # NOTE: raise is not an re-raise, but an error with a different message
        # pylint: disable=raise-missing-from
        raise ValueError(f"Optional prefix ({OPTIONAL_PREFIX}) was not found in token.")
    return token[prefixPos + 1:]


def tokenize(sentence: str):
    """Tokenize a sentence, splitting it into indiviual tokens (words)."""

    return sentence.split()


def isCategory(token: str) -> bool:
    """Determine if a token is a category."""

    return (
        token.startswith(CATEGORY_PREFIX) or (
            token.startswith(OPTIONAL_PREFIX) and
            token[len(OPTIONAL_PREFIX):].startswith(CATEGORY_PREFIX)
        )
    )


def isOptional(label: str) -> bool:
    """Determine if a token is marked as optional."""
    return label.startswith(OPTIONAL_PREFIX) and len(label) > 1


def cellIsFull(chart, i, j):
    """Determine if a cell has hit its capacity in the chart based parsing algorithm."""
    return len(chart[(i, j)]) >= MAX_CELL_CAPACITY


def getResourcePath():
    """Return the path to the used resource folder."""
    return os.path.join(os.path.dirname(__file__), "./resources")


def loadResource(identifier: str):
    """
    Load the resource specified by the identifier.

    The identifier is the name of the file without any prefixed path information
    (e.g. directories) and the suffixed file ending of '.p'.
    """
    filepath = os.path.join(getResourcePath(), f"{identifier}.p")

    try:
        with open(filepath, "rb") as resourceFile:
            obj = pickle.load(resourceFile)
    except FileNotFoundError:
        obj = None

    return obj


def saveResource(obj: Any, identifier: str):
    """
    Save a resource with the given identifier.

    Resources saved in this way can be loaded using `loadResource`.
    """
    filepath = os.path.join(getResourcePath(), f"{identifier}.p")
    return pickle.dump(obj, open(filepath, "wb"))


def trainingVectorsFromDenotations(
        trainingData,
        parseFunc,
        evaluateFunc,
        scoreFunc,
        toFeatureVecFunc):
    """Create training vectors from the given training data."""

    trainingInputVectors = []
    trainingOutput = []

    for data in trainingData:
        parses = parseFunc(data)
        for parse in parses:
            evaluated = evaluateFunc(parse)
            score = scoreFunc(evaluated, data)
            trainingOutput.append(score)
            trainingInputVectors.append(toFeatureVecFunc(parse))

    return trainingInputVectors, trainingOutput


def makeLruCached(function, maxsize: int = 128):
    "Create an LRU cached version of the given function with given max. cache size."

    @lru_cache(maxsize=maxsize)
    def cachedFunction(*args, **kwargs):
        return function(*args, **kwargs)

    return cachedFunction
