"""
Class module for the `DanfoSemanticEvaluator` class.
Please see `help(DanfoSemanticEvaluator)` for more information.
"""
import re

from typing import Any, Optional, Tuple
from ..parse import Parse

OPERATOR_TO_FUNCTION = {
    ">": "gt",
    ">=": "ge",
    "<": "lt",
    "<=": "le",
    "==": "eq",
    "!=": "ne"
}


class DanfoSemanticEvaluator:
    """Evaluator for generating danfo snippets from semantics."""
    # NOTE: class could theoretically be a singleton, this doesn't create a significant advantage
    #   though and is only slightly cleaner
    # pylint: disable=no-self-use
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def evaluate(self, parse: Parse) -> str:
        """Generate a danfo.js snippet from the semantics of the given parse."""
        snippet = None
        semantics = parse.semantics
        if "domain" in semantics and semantics["domain"] == "danfo":
            if "operation" in semantics:
                operation = semantics["operation"]
                # create operation
                if operation == "create":
                    snippet = self._makeCreateSnippet(semantics)

                # select operation
                elif operation == "select":
                    snippet = self._makeSelectSnippet(semantics)

                # convert operation
                elif operation == "convert":
                    snippet = self._makeConvertSnippet(semantics)

                # append operation
                elif operation == "append":
                    snippet = self._makeAppendSnippet(semantics)

                elif operation == "sort":
                    snippet = self._makeSortSnippet(semantics)

                elif operation == "read":
                    snippet = self._makeReadSnippet(semantics)

                elif operation == "group":
                    snippet = self._makeGroupSnippet(semantics)

        return snippet

    def _getRangeArgument(self, rangeSemantics: dict) -> Optional[Tuple[Any, Any]]:
        # handle range semantics
        rangeArgument = None
        if rangeSemantics is not None:
            rangeFromSemantics = rangeSemantics.get("from", None)
            rangeFrom = None
            if rangeFromSemantics is not None:
                rangeFrom = rangeFromSemantics.get("value", None)

            rangeToSemantics = rangeSemantics.get("to", None)
            rangeTo = None
            if rangeToSemantics is not None:
                rangeTo = rangeToSemantics.get("value", None)

            # ensure that rangeFrom is smaller than rangeTo, swap them if necessary
            if rangeFrom is not None and rangeTo is not None and rangeFrom > rangeTo:
                rangeFrom, rangeTo = rangeTo, rangeFrom

            rangeArgument = (rangeFrom, rangeTo)

        return rangeArgument

    def _getCondition(self, conditionSemantics: dict, sourceVariable: str) -> Optional[str]:
        condition = None
        if conditionSemantics is not None:
            # NOTE: If one of the sides is a variable we assume that
            # it specifies a column name of the source dataframe
            lhs = conditionSemantics.get("lhs", {})
            lhsExpression = "%lhs%"
            if "variable" in lhs:
                lhsExpression = f"{sourceVariable}[{lhs['variable']}]"
            elif "value" in lhs:
                lhsExpression = lhs["value"]

            rhs = conditionSemantics.get("rhs", {})
            rhsExpression = "%rhs%"
            if "variable" in rhs:
                rhsExpression = f"{sourceVariable}[{rhs['variable']}]"
            elif "value" in rhs:
                rhsExpression = rhs["value"]
            else:
                rhsExpression = "%rhs%"

            operator = conditionSemantics.get("operator", "%operator%")
            operatorFunction = OPERATOR_TO_FUNCTION[operator]

            condition = f"{lhsExpression}.{operatorFunction}({rhsExpression})"

        return condition

    def _makeCreateSnippet(self, semantics: dict) -> str:
        typeArgument = semantics.get("type", None)
        fromArgument = semantics.get("from", {}).get("variable", "")
        assignTo = semantics.get("assignTo", {}).get("variable", None)
        if assignTo is None:
            assignTo = semantics.get("typeVariable", {}).get("variable", None)

        if typeArgument is None:
            typeArgument = "%class%"

        if fromArgument is None:
            fromArgument = ""

        if "index" in semantics:
            if semantics["index"] is None:
                index = "%index%"
            else:
                index = semantics["index"]["token"]

            fromArgument += f", {{ index: {index} }}"

        snippet = f"new danfo.{typeArgument}({fromArgument})"

        if assignTo is not None and snippet is not None:
            snippet = f"let {assignTo} = {snippet}"

        return snippet

    def _makeSelectSnippet(self, semantics: dict) -> str:
        selectType = semantics.get("selectType", None)
        assignTo = semantics.get("assignTo", {}).get("variable", None)

        snippet = None
        if selectType == "column":
            snippet = self._makeSelectColumnSnippet(semantics)
        elif selectType == "row":
            snippet = self._makeSelectRowSnippet(semantics)
        elif selectType == "maximum":
            snippet = self._makeSelectMaximumSnippet(semantics)
        elif selectType == "minimum":
            snippet = self._makeSelectMinimumSnippet(semantics)
        elif selectType == "average":
            snippet = self._makeSelectAverageSnippet(semantics)

        if assignTo is not None and snippet is not None:
            snippet = f"let {assignTo} = {snippet}"

        return snippet

    def _makeGroupSnippet(self, semantics: dict) -> str:
        sourceVariable = semantics.get("typeVariable", {}).get("variable", None)
        groupBy = self._getSymbolsRepresentation(semantics.get("by", {})) or "%col%"
        assignTo = semantics.get("assignTo", {}).get("variable", None)

        if sourceVariable is None:
            sourceVariable = "%dataframe%"

        if not isinstance(groupBy, list):
            groupBy = [groupBy]

        groupBy = "[" + ", ".join(groupBy) + "]"
        snippet = f"{sourceVariable}.groupby({groupBy})"

        if assignTo is not None and snippet is not None:
            snippet = f"let {assignTo} = {snippet}"

        return snippet

    def _makeReadSnippet(self, semantics: dict) -> str:
        readFormat = semantics.get("readFormat", {}).get("format", None)
        source = semantics.get("from", {}).get("variable", None)
        assignTo = semantics.get("assignTo", {}).get("variable", "df")

        if source is not None:
            source = f'"{source}"'
        else:
            source = "%source%"

        readMethod = {
            "csv": "readCSV",
            "excel": "readExcel",
            "json": "readJSON"
        }.get(readFormat)

        if readMethod is not None:
            return f"let {assignTo} = await danfo.{readMethod}({source})"

        return None

    def _makeSelectColumnSnippet(self, semantics: dict) -> str:
        # sourceType = semantics.get("selectSource", {}).get("type", None)
        sourceVariable = (
            semantics.get("selectSource", {})
                     .get("typeVariable", {})
                     .get("variable", "%dataframe%")
        )
        columnNameSemantics = semantics.get("columnName", None)

        columnName = self._getSymbolsRepresentation(columnNameSemantics)
        if columnName is None:
            columnName = "%column%"

        if isinstance(columnName, list):
            # select multiple columns
            columnNames = "[" + ", ".join(columnName) + "]"
            columnSelectionSnippet = f".loc({{ columns: {columnNames} }})"
        else:
            columnSelectionSnippet = f"[{columnName}]"

        snippet = f"{sourceVariable}{columnSelectionSnippet}"
        return snippet

    def _makeSelectRowSnippet(self, semantics: dict) -> str:
        # NOTE: could be refactored - split into smaller sub functions
        # pylint: disable=too-many-locals
        sourceVariable = (
            semantics.get("selectSource", {})
                     .get("typeVariable", {})
                     .get("variable", "%dataframe%")
        )
        conditionSemantics = semantics.get("condition", None)
        rangeSemantics = semantics.get("range", None)
        headSemantics = semantics.get("head", None)
        tailSemantics = semantics.get("tail", None)

        # handle condition semantics
        condition = self._getCondition(conditionSemantics, sourceVariable)

        # handle range semantics
        rangeArgument = self._getRangeArgument(rangeSemantics)

        head = None
        if headSemantics is not None:
            head = headSemantics.get("value", None)

        tail = None
        if tailSemantics is not None:
            tail = tailSemantics.get("value", None)

        # build snippet
        snippet = f"{sourceVariable}"

        if rangeArgument is not None:
            rangeFromSnippet = rangeArgument[0] if rangeArgument[0] is not None else ""
            rangeToSnippet = rangeArgument[1] if rangeArgument[1] is not None else ""
            snippet += f".iloc({{ rows: ['{rangeFromSnippet}:{rangeToSnippet}'] }})"

        if condition is not None:
            snippet += f".iloc({{ rows: {condition} }})"

        if head is not None:
            snippet += f".head({head})"
        elif tail is not None:  # do not apply tail if a head condition exists
            snippet += f".tail({tail})"

        if condition is None and rangeArgument is None and head is None and tail is None:
            snippet += ".iloc({ rows: %condition% })"

        return snippet

    def _makeSelectMaximumSnippet(self, semantics: dict) -> str:
        selectSource = (semantics.get("selectSource", {})
                                 .get("typeVariable", {})
                                 .get("variable", "%dataframe%"))
        subSelectSource = (semantics.get("subSelectSource", {})
                                    .get("typeVariable", {})
                                    .get("variable", None))

        if subSelectSource is None and selectSource is not None:
            snippet = f"{selectSource}.max()"
        else:
            if subSelectSource is None:
                subSelectSource = "%column%"
            snippet = f"{selectSource}[{subSelectSource}].max()"

        return snippet

    def _makeSelectMinimumSnippet(self, semantics: dict) -> str:
        selectSource = (semantics.get("selectSource", {})
                                 .get("typeVariable", {})
                                 .get("variable", "%dataframe%"))
        subSelectSource = (semantics.get("subSelectSource", {})
                                    .get("typeVariable", {})
                                    .get("variable", None))

        if subSelectSource is None and selectSource is not None:
            snippet = f"{selectSource}.min()"
        else:
            if subSelectSource is None:
                subSelectSource = "%column%"
            snippet = f"{selectSource}[{subSelectSource}].min()"

        return snippet

    def _makeSelectAverageSnippet(self, semantics: dict) -> str:
        selectSource = (semantics.get("selectSource", {})
                                 .get("typeVariable", {})
                                 .get("variable", "%dataframe%"))
        subSelectSource = (semantics.get("subSelectSource", {})
                                    .get("typeVariable", {})
                                    .get("variable", None))

        if subSelectSource is None and selectSource is not None:
            snippet = f"{selectSource}.mean()"
        else:
            if subSelectSource is None:
                subSelectSource = "%column%"
            snippet = f"{selectSource}[{subSelectSource}].mean()"

        return snippet

    def _makeConvertSnippet(self, semantics: dict) -> str:
        convertFormat = semantics.get("convertFormat", {}).get("format", None)
        convertOrder = semantics.get("convertOrder", None)
        sourceVariable = semantics.get("typeVariable", {}).get("variable", None)
        assignTo = semantics.get("assignTo", {}).get("variable", None)

        if sourceVariable is None:
            sourceVariable = "%dataframe%"

        convertArgs = ""
        if convertOrder is not None:
            convertArgs = "{format: \"row\"}"

        snippet = None
        if convertFormat == "csv":
            snippet = f"{sourceVariable}.toCSV({convertArgs})"
        elif convertFormat == "json":
            snippet = f"{sourceVariable}.toJSON({convertArgs})"

        if assignTo is not None and snippet is not None:
            snippet = f"let {assignTo} = {snippet}"

        return snippet

    def _makeAppendSnippet(self, semantics: dict) -> str:
        appendType = semantics.get("appendType", "row")
        appendSourceTarget = semantics.get("appendSourceTarget", {})
        appendSource = appendSourceTarget.get("source", {}).get("variable", "%dataframe1%")
        appendTarget = appendSourceTarget.get("target", {}).get("variable", "%dataframe2%")
        assignTo = semantics.get("assignTo", {}).get("variable", None)

        snippet = None
        if appendType == "row":
            snippet = f"danfo.concat({{ dfList: [{appendSource}, {appendTarget}], axis: 0 }})"
        elif appendType == "column":
            snippet = f"danfo.concat({{ dfList: [{appendSource}, {appendTarget}], axis: 1 }})"

        if assignTo is not None and snippet is not None:
            snippet = f"let {assignTo} = {snippet}"

        return snippet

    def _makeSortSnippet(self, semantics: dict) -> str:
        sourceVariable = semantics.get("typeVariable", {}).get("variable", None)
        orderBy = self._getSymbolsRepresentation(semantics.get("by", {})) or "%col%"
        assignTo = semantics.get("assignTo", {}).get("variable", None)

        if sourceVariable is None:
            sourceVariable = "%dataframe%"

        if isinstance(orderBy, list):
            orderBy = "[" + ", ".join(orderBy) + "]"

        snippet = f"{sourceVariable}.sortValues({orderBy}, {{ inplace: true }})"

        if assignTo is not None and snippet is not None:
            snippet = f"let {assignTo} = {snippet}"

        return snippet

    def _getSymbolsRepresentation(self, symbolSemantics: dict) -> Optional[str]:
        """
        Take in the semantics dict of a symbols dictionary (which contains one or multiple symbols,
        either all of type variable or all of type literal) and return a representation of these
        symbols (variable name is returned as is, literal is returned in quotes)
        """
        if (
            symbolSemantics is None
            or "variable" not in symbolSemantics
            and "literal" not in symbolSemantics
        ):
            return None

        # Any symbol is either a literal or a variable
        isLiteral = "literal" in symbolSemantics
        symbolOrSymbols = (
            symbolSemantics["literal"] if isLiteral else symbolSemantics["variable"]
        )

        # We might have either a list of symbols or only one symbol,
        # and each symbol can be valid or invalid
        # (i.e. be None after normalization or not None).
        if isinstance(symbolOrSymbols, list):
            normalizedSymbols = [
                self._normalizeSymbol(symbol, isLiteral) for symbol in symbolOrSymbols
            ]
            filteredNormalizedSymbols = [
                symbol for symbol in normalizedSymbols if symbol is not None
            ]

            if len(filteredNormalizedSymbols) > 0:
                return filteredNormalizedSymbols

            return None

        return self._normalizeSymbol(symbolOrSymbols, isLiteral)

    def _normalizeSymbol(self, symbol: str, isLiteral: bool) -> Optional[str]:
        """
        Normalize the representation of the given symbol:

        - If it is a literal, ensure it is in single quotes
        - If it is a variable (= not a literal), ensure it is unquoted
        - Return None if the symbol cannot be properly
          normalized given the type (literal or variable)
        """

        # Check if symbol is in quotes
        quoteMatch = re.fullmatch(r"[\"'](.*)[\"'],?", symbol)
        if quoteMatch is not None:
            quoteMatch = quoteMatch[1]

        if isLiteral and quoteMatch is None:
            # Symbol is a literal and not quoted, so we quote it
            return f"'{symbol}'"

        if isLiteral and quoteMatch is not None:
            # Symbol is a literal and quoted, normalize quotes
            return f"'{quoteMatch}'"

        if not isLiteral and quoteMatch is None:
            # Symbol is a variable and not quoted, just return it
            return symbol

        # Symbol is a variable but quoted - you cannot quote variables, so just return None
        return None
