import pytest
from collections import namedtuple

from semantic_pandas.evaluators import DanfoSemanticEvaluator

DummyParse = namedtuple("DummyParse", "semantics")


class TestDanfoSemanticEvaluator:
    @pytest.mark.parametrize("semantics, expectedSnippet", [
        # create operation
        (
            DummyParse({
                "domain": "danfo",
                "operation": "create",
                "type": "Dataframe",
                "from": {"variable": "dict"},
                "index": {"token": "index"},
                "assignTo": {"variable": "df"}
            }),
            "let df = new danfo.Dataframe(dict, { index: index })"
        ),
        # select operations
        (
            DummyParse({
                "domain": "danfo",
                "operation": "select",
                "selectType": "column",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "columnName": {"variable": "col"},
                "assignTo": {"variable": "result"}
            }),
            "let result = df[col]"
        ),
        (
            DummyParse({
                "domain": "danfo",
                "operation": "select",
                "selectType": "row",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "condition": {
                    "lhs": {"variable": "var"},
                    "rhs": {"value": "3"},
                    "operator": ">"
                },
                "assignTo": {"variable": "result"}
            }),
            "let result = df.iloc({ rows: df[var].gt(3) })"
        ),
        (
            DummyParse({
                "domain": "danfo",
                "operation": "select",
                "selectType": "row",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "head": {"value": 10},
                "assignTo": {"variable": "result"}
            }),
            "let result = df.head(10)"
        ),
        (
            DummyParse({
                "domain": "danfo",
                "operation": "select",
                "selectType": "row",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "tail": {"value": 10},
                "assignTo": {"variable": "result"}
            }),
            "let result = df.tail(10)"
        ),
        (
            DummyParse({
                "domain": "danfo",
                "operation": "select",
                "selectType": "row",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "range": {
                    "from": {"value": 0},
                    "to": {"value": 10}
                },
                "assignTo": {"variable": "result"}
            }),
            "let result = df.iloc({ rows: ['0:10'] })"
        ),
        (
            DummyParse({
                "domain": "danfo",
                "operation": "select",
                "selectType": "maximum",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "subSelectSource": {"typeVariable": {"variable": "col"}},
                "assignTo": {"variable": "result"}
            }),
            "let result = df[col].max()"
        ),
        (
            DummyParse({
                "domain": "danfo",
                "operation": "select",
                "selectType": "minimum",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "subSelectSource": {"typeVariable": {"variable": "col"}},
                "assignTo": {"variable": "result"}
            }),
            "let result = df[col].min()"
        ),
        (
            DummyParse({
                "domain": "danfo",
                "operation": "select",
                "selectType": "average",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "subSelectSource": {"typeVariable": {"variable": "col"}},
                "assignTo": {"variable": "result"}
            }),
            "let result = df[col].mean()"
        ),
        # convert operations
        (
            DummyParse({
                "domain": "danfo",
                "operation": "convert",
                "convertFormat": {"format": "csv"},
                "typeVariable": {"variable": "df"},
                "assignTo": {"variable": "result"}
            }),
            "let result = df.toCSV()"
        ),
        # append operations
        (
            DummyParse({
                "domain": "danfo",
                "operation": "sort",
                "typeVariable": {"variable": "df"},
                "by": {"variable": "col"},
                "assignTo": {"variable": "result"}
            }),
            "let result = df.sortValues(col, { inplace: true })"
        ),
        (DummyParse({"domain": "other"}), None)
    ])
    def testEvaluate(self, semantics, expectedSnippet):
        evaluator = DanfoSemanticEvaluator()

        evaluatedSnippet = evaluator.evaluate(semantics)

        assert evaluatedSnippet == expectedSnippet
