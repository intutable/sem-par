from collections import namedtuple

from semantic_pandas import Parse

DummySemantic = namedtuple("DummySemantic", "compute")


class DummyRule:
    semantic = DummySemantic(lambda x: None)

    def __init__(self, identifier):
        self.identifier = identifier

    def __str__(self):
        return self.identifier


class TestParse:
    def testRuleFeatures(self):
        dummyRule = DummyRule("DummyRule")

        parse = Parse(dummyRule, [])

        ruleFeatures = parse.ruleFeatures()

        assert ruleFeatures[str(dummyRule)] == 1

    def testRulePrecedence(self):
        dummyRule1 = DummyRule("DummyRule1")
        dummyRule2 = DummyRule("DummyRule2")

        parse1 = Parse(dummyRule1, [])
        parse2 = Parse(dummyRule2, [parse1])

        rulePrecedenceFeatures = parse2.rulePrecedenceFeatures()

        assert rulePrecedenceFeatures[(str(dummyRule2), str(dummyRule1))] == 1
