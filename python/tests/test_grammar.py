from semantic_pandas import Grammar
from semantic_pandas.rules import LexicalRule, UnaryRule, BinaryRule, NAryRule
from semantic_pandas.semantics import (SimpleSemantic, ProxySemantic, CombineSemantic,
                                       CustomSemantic)


class TestGrammar:
    def testParse(self):
        rules = [
            LexicalRule("$Lexical", "Lexical", SimpleSemantic({"value": "Lexical"})),
            UnaryRule("$Unary", "$Lexical", ProxySemantic(0)),
            BinaryRule("$Binary", "$Unary $Unary", CombineSemantic([0, 1]))
        ]

        grammar = Grammar(rules)
        parses = grammar.parse("Lexical Lexical")
        assert len(parses) == 1
        assert parses[0].rule == rules[2]

    def testAddNAryRuleAndParse(self):
        rules = [
            LexicalRule("$A", "A", SimpleSemantic({"value": "A"})),
            LexicalRule("$B", "B", SimpleSemantic({"value": "B"})),
            LexicalRule("$C", "C", SimpleSemantic({"value": "C"})),
            LexicalRule("$D", "D", SimpleSemantic({"value": "D"})),
            LexicalRule("$E", "E", SimpleSemantic({"value": "E"})),
            NAryRule("$NAry", ["$A", "$B", "$C", "$D", "$E"], CustomSemantic(lambda sems: sems))
        ]
        grammar = Grammar(rules)
        parses = grammar.parse("A B C D E")

        for rule in grammar.rules:
            print(f"{rule.lhs} {rule.rhs}")
        assert len(parses) == 1
        assert parses[0].semantics == [
            {"value": "A"},
            {"value": "B"},
            {"value": "C"},
            {"value": "D"},
            {"value": "E"},
        ]

    def testRuleWithOptional(self):
        rules = [
            LexicalRule("$ROOT", "lexical ?optional", SimpleSemantic(0))
        ]

        grammar = Grammar(rules, startSymbol="$ROOT")
        # parses with optional
        parses = grammar.parse("lexical")
        assert len(parses) == 1
        assert parses[0].semantics == 0

        # parses with optional
        parses = grammar.parse("lexical optional")
        assert len(parses) == 1
        assert parses[0].semantics == 0

    def testRuleFeatureDict(self):
        rules = [
            LexicalRule("$A", "A"),
            LexicalRule("$B", "B"),
            BinaryRule("$C", ["$A", "$B"])
        ]
        grammar = Grammar(rules)

        ruleFeatureDict = grammar.ruleFeatureDict

        for rule in rules:
            assert str(rule) in ruleFeatureDict and ruleFeatureDict[str(rule)] == 0

        for rule1 in rules:
            for rule2 in rules:
                if rule2.lhs in rule1.rhs:
                    assert (str(rule1), str(rule2)) in ruleFeatureDict \
                        and ruleFeatureDict[(str(rule1), str(rule2))] == 0

    def testRuleFeatureDictImmutable(self):
        rules = [
            LexicalRule("$A", "A"),
            LexicalRule("$B", "B"),
            BinaryRule("$C", ["$A", "$B"])
        ]
        grammar = Grammar(rules)

        ruleFeatureDict = grammar.ruleFeatureDict

        ruleFeatureDict[str(rules[0])] = 1
        grammar.ruleFeatureDict[str(rules[0])] = 1

        assert ruleFeatureDict != grammar.ruleFeatureDict
