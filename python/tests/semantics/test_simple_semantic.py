from semantic_pandas.semantics import SimpleSemantic


class TestSimpleSemantic:
    def testCompute(self):
        semanticValue = {"value": "key"}
        semantic = SimpleSemantic(semanticValue)

        assert semantic.compute(None) == semanticValue
        assert semantic.compute([]) == semanticValue
        assert semantic.compute([None]) == semanticValue
        assert semantic.compute([{}]) == semanticValue
        assert semantic.compute([{}, {}]) == semanticValue
