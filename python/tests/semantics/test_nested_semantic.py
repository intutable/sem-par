import pytest

from semantic_pandas.semantics import NestedSemantic


class TestNestedSemantic:
    @pytest.mark.parametrize("labelMapping, semanticValues, computedValues", [
        ({"label": 0}, [{"value": "key"}], {"label": {"value": "key"}}),
        (
            {"label1": 0, "label2": 0},
            [{"value": "key"}],
            {"label1": {"value": "key"}, "label2": {"value": "key"}}
        ),
        (
            {"label1": 0, "label2": 1},
            [{"value1": "key1"}, {"value2": "key2"}],
            {"label1": {"value1": "key1"}, "label2": {"value2": "key2"}}
        ),
        (
            {"label1": 1, "label2": 0},
            [{"value1": "key1"}, {"value2": "key2"}],
            {"label1": {"value2": "key2"}, "label2": {"value1": "key1"}}
        ),
        (
            {"label": 0},
            [{"value1": "key1"}, {"value2": "key2"}],
            {"label": {"value1": "key1"}}
        )
    ])
    def testCompute(self, labelMapping, semanticValues, computedValues):
        semantic = NestedSemantic(labelMapping)

        assert semantic.compute(semanticValues) == computedValues

    @pytest.mark.parametrize("labelMapping, semanticValues", [
        ({"label": 1}, [{"value": "key"}])
    ])
    def testComputeInvalid(self, labelMapping, semanticValues):
        semantic = NestedSemantic(labelMapping)

        with pytest.raises(IndexError):
            semantic.compute(semanticValues)
