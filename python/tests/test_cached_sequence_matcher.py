import pytest

from semantic_pandas import CachedSequenceMatcher


class TestCachedSequenceMatcher:
    @pytest.mark.parametrize("tokens1, tokens2, threshold, areSimilar", [
        ("dog", "dog", 1, True),
        ("dog", "cat", 1, False),
        ("dog", "cat", 0, True),
    ])
    def testSequencesAreSimilar(self, tokens1, tokens2, threshold, areSimilar):
        sequenceMatcher = CachedSequenceMatcher(1, 1)
        assert sequenceMatcher.similar(tokens1, tokens2, threshold) == areSimilar
