from semantic_pandas.rules import NAryRule, BinaryRule
from semantic_pandas.semantics import CustomSemantic


class TestNAryRule:

    def testBinarize(self):
        rule = NAryRule("$NAry", ["$A", "$B", "$C"], CustomSemantic(lambda sems: sems))

        lhsBinarizedRule, rhsBinarizedRule = rule.binarize()

        assert isinstance(lhsBinarizedRule, BinaryRule)
        assert lhsBinarizedRule.lhs == "$NAry"
        assert lhsBinarizedRule.rhs[0] == ("$A")

        assert isinstance(rhsBinarizedRule, BinaryRule)
        assert rhsBinarizedRule.rhs == ("$B", "$C")

        assert ["$B", "$C"] == rhsBinarizedRule.semantic.compute(["$B", "$C"])
        assert ["$A", "$B", "$C"] == lhsBinarizedRule.semantic.compute(["$A", ["$B", "$C"]])
