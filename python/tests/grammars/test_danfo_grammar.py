import pytest

from semantic_pandas.datasets import manualDanfoQueries
from semantic_pandas.grammars import danfoGrammar
from semantic_pandas.evaluators import DanfoSemanticEvaluator


class TestDanfoGrammar:
    @pytest.mark.parametrize("query", manualDanfoQueries)
    def testDanfoGrammarCanParseAllDatasetQueries(self, query):
        evaluator = DanfoSemanticEvaluator()

        intent = query.intent
        snippet = query.snippet

        parses = danfoGrammar.parse(intent)
        for parse in parses:
            evaluatedSnippet = evaluator.evaluate(parse)
            if evaluatedSnippet == snippet:
                break
        else:
            pytest.fail(f"No matching snippet found for '{snippet}' with intent '{intent}'")
