import pytest
from semantic_pandas.intutable import IntutableDanfoParser


class TestIntutableDanfoParser:
    @pytest.mark.parametrize(
        "utterance, snippet, expectedSimilarSnippet",
        [
            (
                "get minimum from col in df as min",
                "df[col].min()",
                "let min = %dataframe%.min()"
            )
        ]
    )
    def testGetSimilarSnippets(self, utterance: str, snippet: str, expectedSimilarSnippet: str):
        parser = IntutableDanfoParser()
        scoredSimilarSnippets = parser.getSimilarSnippets(utterance, snippet)
        similarSnippets = [s[0] for s in scoredSimilarSnippets]

        assert any(expectedSimilarSnippet == s for s in similarSnippets)
        assert all(snippet != s for s in similarSnippets)

    @pytest.mark.parametrize(
        "utterance, similarityThreshold",
        [
            ("obtain lowest", 0.5),
            ("generate dataframe", 0.5)
        ]
    )
    def testParseAllWithSimilarity(self, utterance: str, similarityThreshold: float):
        # NOTE: the utterance should contain atleast one word that is NOT
        # part of a lexical rule of the parser grammar
        parser = IntutableDanfoParser()
        scoredSnippets = parser.parseAll(utterance, similarityThreshold)

        # One parse is always generated (default "other" parse), so we
        # require that atleast two parses must be generated
        assert len(scoredSnippets) >= 2

    @pytest.mark.parametrize(
        "utterance",
        [
            ("obtain lowest"),
            ("generate dataframe")
        ]
    )
    def testParseAllWithoutSimilarity(self, utterance: str):
        parser = IntutableDanfoParser()
        scoredSnippets = parser.parseAll(utterance, 1)

        # Should only generate default parse
        assert len(scoredSnippets) == 1
