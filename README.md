# Semantic Parsing for xTable

The `sem-par` plugin for xTable (also known as intutable) converts natural language utterances to danfo.js data analysis code.

[[_TOC_]]

## ▶ 1-click demo

See our ["1-Click Demo" documentation](notes/1_click_demo.md) for details.

## 🔧 Prerequisites and setup

To build something that uses the `sem-par` plugin (probably another plugin, e.g. one that provides a GUI for xTable) you first need a working Node.js + TypeScript environment. The guide ["How to create a plugin"][howto-plugin] from the xTable documentation might be helpful. For now, we'll assume you already have such an environment set up.

Install the `@intutable/sem-par` package with the following command:

```
npm install @intutable/sem-par
```

Because most the actual parsing code is written in Python, you also need a working Python environment. You can install all the Python code for `sem-par` in a virtual environment with the following commands:

```
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip setuptools wheel
pip install -e node_modules/@intutable/sem-par/python
```

Note that you need to execute `source venv/bin/activate` every time before running any TypeScript script that uses `sem-par` (or, more precisely, once per shell session).
To ensure that that the correct Python executable is used, you might need to set the `PYTHON` environment variable to the path of the Python executable e.g. in bash execute `PYTHON=venv/bin/python` .

<details>
<summary>Alternative installation without a <code>venv</code></summary>

If you are fine with globally installing the Python packages, you don't have to create a virtual environment ("venv" for short). In this case, you do not have to execute `source venv/bin/activate` ever, and the commands required for setup are only:

```
pip install --upgrade pip setuptools wheel
pip install -e node_modules/@intutable/sem-par/python
```

</details>

## 🏃 Usage

Just as with any other xTable plugin (see the [architecture documentation][architecture] for details), you communicate with `sem-par` by sending requests and receiving responses via a `Core` instance. To make sending a request a little easier, we provide so called "request builder functions", which can be used like this:

```ts
import path from "path"
import { Core } from "@intutable/core"
import { getSuggestions } from "@intutable/sem-par/dist/requests"
import { SuggestionsResponse } from "@intutable/sem-par/dist/types"

async function main() {
    const core = await Core.create(
        // Load the sem-par plugin, assuming the node_modules directory
        // resides one directory level above this file
        [path.join(__dirname, "../node_modules/@intutable/sem-par")]
    )

    // Use a request builder function to build a "getSuggestions" request
    const request = getSuggestions("sort by name", 2)
    // Make the request, await the response and cast it to the correct type
    const response = (await core.events.request(request)) as SuggestionsResponse
    console.log(response.suggestions)
}

main()
```

The output of this script will be something like this:

```js
[
    {
        snippet: "%dataframe%.sortValues(name, { inplace: true })",
        score: 1
    },
    {
        snippet: "%dataframe%.sortValues('name', { inplace: true })",
        score: 0.013333333333333332
    }
]
```

## 🔌 API

Our API is defined in the files `requests.ts` and `types.ts`, for which documentation is provided below:

* [Request builder functions][docs-requests]
* [Request and response types][docs-types]

## 👥 Contributing

See our [contributing guide][contributing-guide] for details. This also explains how to set up a development environment and outlines the basic structure of our code and how to extend it, so make sure to check it out.

## 🏁 Release strategy

This project uses [Semantic Versioning][semver]. Releases are available in the [xTable Package Registry][sem-par-packages] and created automatically from the `main` branch.

To release a version, first open a MR from a feature branch to `develop` that increases the version number and have it approved and merged. Then, open a MR from `develop` to `main` and have it approved and merged by one of the xTable administrators.

## 🏦 Accounting

See the [accounting documentation](notes/accounting.md) for information on who did what in the project.

## ⚖️ License

This plugin is licensed under the [Apache License, Version 2.0][apache2].

[semver]: https://semver.org
[apache2]: https://www.apache.org/licenses/LICENSE-2.0
[howto-plugin]: https://intutable.gitlab.io/plugins/newplugin/
[sem-par-packages]: https://gitlab.com/groups/intutable/-/packages?type=&orderBy=name&sort=desc&search[]=%40intutable%2Fsem-par&search[]=
[architecture]: https://intutable.gitlab.io/documentation/architecture/
[docs-requests]: https://intutable.gitlab.io/sem-par/modules/requests.html
[docs-types]: https://intutable.gitlab.io/sem-par/modules/types.html
[contributing-guide]: CONTRIBUTING.md
